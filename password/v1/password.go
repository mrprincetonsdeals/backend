package v1

import (
	"crypto/rand"
	"crypto/subtle"
	"errors"

	"golang.org/x/crypto/argon2"
)

const (
	saltLen           = 8
	iterations uint32 = 1
	memory     uint32 = 64 * 1024
	threads    uint8  = 1
	keyLen     uint32 = 32
)

var (
	EmptySaltErr error = errors.New("invalid salt length")
)

func ComparePasswordToHash(passwordHash, hash []byte) bool {
	return subtle.ConstantTimeCompare(passwordHash, hash) == 1
}

func GenerateSalt() ([]byte, error) {
	buf := make([]byte, saltLen)

	_, err := rand.Read(buf)
	if err != nil {
		return nil, err
	}

	return buf, nil
}

func Hash(password, salt []byte) ([]byte, error) {
	var err error

	if len(salt) == 0 {
		err = EmptySaltErr
	}

	key := argon2.IDKey(password, salt, iterations, memory, threads, keyLen)

	return key, err
}
