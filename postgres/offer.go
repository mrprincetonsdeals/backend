package postgres

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/lib/pq"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

// OffersGetRecommended retrieves recommended offers
func (db *Datastore) OffersGetRecommended() ([]*models.Offer, error) {
	rows, err := db.DB.Query(`SELECT offer_id, company_id FROM recommended_offers;`)
	if err != nil {
		return nil, err
	}

	var (
		offerIDs   []string
		companyIDs []string
	)

	for rows.Next() {
		var (
			offerID   string
			companyID string
		)

		if err := rows.Scan(&offerID, &companyID); err != nil {
			return nil, err
		}

		offerIDs = append(offerIDs, offerID)
		companyIDs = append(companyIDs, companyID)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	rows, err = db.DB.Query(`SELECT id, name, description FROM companies WHERE id = ANY($1);`, pq.Array(companyIDs))
	if err != nil {
		return nil, err
	}

	companies := map[string]*models.Company{}
	for rows.Next() {
		company := &models.Company{}

		if err := rows.Scan(&company.ID, &company.Name, &company.Description); err != nil {
			return nil, err
		}

		companies[company.ID] = company
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	rows, err = db.DB.Query(`SELECT id, description, company_id FROM offers WHERE id = ANY($1);`, pq.Array(offerIDs))
	if err != nil {
		return nil, err
	}

	var (
		results []*models.Offer
	)
	for rows.Next() {
		offer := &models.Offer{}

		if err := rows.Scan(&offer.ID, &offer.Description, &offer.CompanyID); err != nil {
			return nil, err
		}

		company, ok := companies[offer.CompanyID]
		if ok {
			offer.Company = company
		}

		results = append(results, offer)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return results, nil
}

// OffersGet retrieves a single offer by its uuid
func (db *Datastore) OffersGet(id string) (*models.Offer, error) {
	var offer models.Offer

	row := db.DB.QueryRow("SELECT id, description, company_id FROM offers WHERE id = $1", id)
	err := row.Scan(&offer.ID, &offer.Description, &offer.CompanyID)

	return &offer, err
}

// OffersGetByURL normalizes the url and checks if it matches any existing company offer url prefixes
func (db *Datastore) OffersGetByURL(dealURL string) (*models.Offer, error) {
	u, err := url.Parse(dealURL)
	if err != nil {
		return nil, err
	}

	normalizedURL := fmt.Sprintf("%s%s", u.Host, u.Path)

	db.mux.RLock()
	offersMatcher := db.OffersMatcher
	db.mux.RUnlock()

	for prefix, offerID := range offersMatcher {
		if strings.HasPrefix(normalizedURL, prefix) {
			return db.OffersGet(offerID)
		}
	}

	return nil, nil
}

// UpdateOffersMap update the offers map
func (db *Datastore) UpdateOffersMap() error {
	rows, err := db.DB.Query("SELECT id, url_prefix FROM offers")
	if err != nil {
		return err
	}

	offersMatcher := map[string]string{}

	for rows.Next() {
		var (
			offerID   string
			urlPrefix string
		)

		if err := rows.Scan(&offerID, &urlPrefix); err != nil {
			return err
		}

		offersMatcher[urlPrefix] = offerID
	}

	if err := rows.Err(); err != nil {
		return err
	}

	db.mux.Lock()
	defer db.mux.Unlock()

	db.OffersMatcher = offersMatcher

	return nil
}
