-- Migration: create-companies
-- Created at: 2018-11-21 16:38:12
-- ====  UP  ====

BEGIN;

  CREATE EXTENSION IF NOT EXISTS pgcrypto;

  CREATE TABLE companies (
    id            uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    name          text NOT NULL,
    safe_name     text UNIQUE NOT NULL,
    description   text NOT NULL,
    logo_url      text,
    created_at    timestamptz NOT NULL DEFAULT now(),
    updated_at    timestamptz NOT NULL DEFAULT now()
  );

COMMIT;

-- ==== DOWN ====

BEGIN;

  DROP TABLE companies;

COMMIT;
