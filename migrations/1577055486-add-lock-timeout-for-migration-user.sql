-- Migration: add-lock-timeout-for-migration-user
-- Created at: 2019-12-22 16:58:06
-- ====  UP  ====

BEGIN;

  ALTER ROLE CURRENT_USER SET lock_timeout = '5s';

COMMIT;

-- ==== DOWN ====

BEGIN;

COMMIT;
