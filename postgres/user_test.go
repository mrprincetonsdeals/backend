package postgres_test

import (
	"sync"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/mrprincetonsdeals/backend/postgres"
)

func TestCreatefriendCode(t *testing.T) {
	tcs := []struct {
		name              string
		code              string
		friendCodeCreator func() (string, error)
		friendCodes       sync.Map
		err               string
	}{
		{
			name: "Success empty friendCode map",
			code: "512abc",
			friendCodeCreator: func() (string, error) {
				return "512abc", nil
			},
		},
		{
			name: "Success duplicate friendCode",
			code: "512abc",
			friendCodeCreator: func() (string, error) {
				return "512abc", nil
			},
		},
		{
			name: "Error friendCodeCreator failure",
			friendCodeCreator: func() (string, error) {
				return "", errors.New("oh no")
			},
			err: "friend code create: oh no",
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			tc := tc
			t.Parallel()

			db := &postgres.Datastore{
				FriendCodeCreator: tc.friendCodeCreator,
			}

			code, err := db.CreateFriendCode()

			if tc.err != "" {
				assert.EqualError(t, err, tc.err)
			} else {
				require.NoError(t, err)

				assert.Equal(t, tc.code, code)
			}
		})
	}
}
