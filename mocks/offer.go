package mocks

import (
	"fmt"
	"math/rand"

	"github.com/google/uuid"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

// NewOffer creates a mock offer
func NewOffer() *models.Offer {
	return &models.Offer{
		ID:          uuid.New().String(),
		Description: fmt.Sprintf("offer-description-%d", rand.Int63()),
		Company:     &models.Company{},
	}
}
