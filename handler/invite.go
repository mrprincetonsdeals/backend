package handler

import (
	"bytes"
	"net/http"
	"net/url"
	"path"

	"github.com/google/jsonapi"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/hlog"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

// InviteGet route that retrieves a code for a user by its uuid
func (h *Handler) InviteGet(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	userID, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Printf("ERROR Unauthorized request %s", err.Error())
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	friendCode, err := h.DB.UsersGetFriendCode(userID)
	if err != nil {
		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	u, err := url.Parse(h.InviteURL)
	u.Path = path.Join(u.Path, string(friendCode))

	payload := []*models.Invite{
		{
			ID:        userID,
			InviteURL: u.String(),
		},
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, payload)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}

	return
}

// InviteGetUser route that retrieves user details for a code
func (h *Handler) InviteGetUser(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	_, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Printf("ERROR Unauthorized request %s", err.Error())
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	friendCode := vars["friendCode"]

	user, err := h.DB.UsersGetByFriendCode(friendCode)
	if err != nil {
		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	if user == nil {
		ll.Error().Err(err).Str("friend_code", friendCode).Msg(DatastoreNotFoundLogMsg)
		http.Error(w, "Not Found", http.StatusNoContent)
		return
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, user)
	if err != nil {
		ll.Error().Err(err).Str("friend_code", friendCode).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}

	return
}

// InviteCreateFriends route that creates a friendship between a user and the owner of the friend code
func (h *Handler) InviteCreateFriends(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	userID, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Printf("ERROR Unauthorized request %s", err.Error())
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	friendCode := vars["friendCode"]

	newFriend, err := h.DB.UsersGetByFriendCode(friendCode)
	if err != nil {
		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	if newFriend == nil {
		ll.Error().Err(err).Str("friend_code", friendCode).Msg(DatastoreNotFoundLogMsg)
		w.WriteHeader(http.StatusNoContent)
		return
	}

	friendID := newFriend.ID

	if userID == friendID {
		ll.Error().Err(err).Str("user_id", userID).Str("friend_id", friendID).Msg(FriendCodeForSelfLogMsg)

		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		w.Write(FriendCodeForSelfResp)
		return
	}

	err = h.DB.FriendsCreate(userID, friendID)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Str("friend_id", friendID).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, newFriend)
	if err != nil {
		ll.Error().Err(err).Str("friend_code", friendCode).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}

	return
}
