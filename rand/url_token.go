package rand

import (
	"crypto/rand"
	"encoding/base64"
)

const (
	byteLength = 6
)

// NewURLSafeToken generate and return a token that can be used in a url
func NewURLSafeToken() (string, error) {
	buf := make([]byte, byteLength)
	_, err := rand.Read(buf)
	if err != nil {
		return "", err
	}

	length := base64.URLEncoding.EncodedLen(byteLength)
	enc := make([]byte, length)
	base64.URLEncoding.Encode(enc, buf)

	return string(enc), nil
}
