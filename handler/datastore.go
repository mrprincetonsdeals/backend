package handler

import (
	"gitlab.com/mrprincetonsdeals/backend/models"
)

// Datastore interface defining the datastore methods
type Datastore interface {
	CodesCreate(userID string, url string) (*models.Code, error)
	CodesGet(id string) (*models.Code, error)
	CompaniesCount() (int, error)
	CompaniesGet(id string) (*models.Company, error)
	CompaniesGetAll(limit, offset int) ([]interface{}, error)
	DealDelete(userID string, dealID string) error
	DealsFindByFriendsWithOfferID(userID string, offerID string) ([]*models.Deal, error)
	DealsFindByUserID(userID string) ([]interface{}, error)
	FriendsCount(userID string) (int, error)
	FriendsCreate(userID string, friendID string) error
	FriendsGet(userID, friendID string) (*models.Friend, error)
	FriendsGetAll(userID string, limit, offset int) ([]interface{}, error)
	OffersGet(id string) (*models.Offer, error)
	OffersGetRecommended() ([]*models.Offer, error)
	Ping() error
	UsersCreate(user *models.User) (string, error)
	UsersGet(email string) (*models.User, error)
	UsersGetByFriendCode(friendCode string) (*models.User, error)
	UsersGetFriendCode(userID string) (string, error)
	UsersVerify(verifyToken string) error
}
