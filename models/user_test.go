package models_test

import (
	"bytes"
	"fmt"

	"github.com/google/jsonapi"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var _ = Describe("User", func() {
	var (
		buf      *bytes.Buffer
		err      error
		userID   string
		username string
	)

	BeforeEach(func() {
		userID = uuid.New().String()
		username = "User's name"

		user := &models.User{
			ID:       userID,
			Username: username,
		}

		buf = bytes.NewBuffer(nil)
		err = jsonapi.MarshalPayload(buf, user)
	})

	It("should not have any errors", func() {
		Expect(err).NotTo(HaveOccurred())
	})

	It("should have the correct type", func() {
		Expect(buf.String()).To(ContainSubstring(`"type":"users"`))
	})

	It("should have the correct id", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"id":"%s"`, userID)))
	})

	It("should have the correct attributes", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"attributes":{"username":"%s"}`, username)))
	})
})
