package handler_test

import (
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strconv"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/rs/zerolog"
	. "github.com/stretchr/testify/mock"
	. "gitlab.com/mrprincetonsdeals/backend/handler"
	"gitlab.com/mrprincetonsdeals/backend/mocks"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var _ = Describe("Offer Handler", func() {
	var (
		handler    *Handler
		datastore  *mocks.Datastore
		authorizer *mocks.Authorizer
		writer     *mocks.Writer
		rw         *httptest.ResponseRecorder
	)

	BeforeEach(func() {
		datastore = &mocks.Datastore{}
		authorizer = &mocks.Authorizer{}
		writer = &mocks.Writer{}
		writer.On("Write", Anything).Return(0, nil)
		logger := zerolog.New(writer)
		inviteURL := "http://example.com/friends/new?invite="

		handler = New(datastore, authorizer, inviteURL, logger)
	})

	Describe("OffersGet", func() {
		var (
			offer  *models.Offer
			userID string
		)

		BeforeEach(func() {
			offer = mocks.NewOffer()
			userID = strconv.FormatInt(rand.Int63(), 10)

			company := mocks.NewCompany()

			deals := []*models.Deal{}
			for i := 0; i < 5; i++ {
				deal := mocks.NewDeal()
				deals = append(deals, deal)
			}

			offer.Company = company
			offer.Deals = deals
		})

		Context("with a valid request", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return(userID, nil)
				datastore.On("OffersGet", AnythingOfType("string")).Return(offer, nil)
				datastore.On("CompaniesGet", AnythingOfType("string")).Return(offer.Company, nil)
				datastore.On("DealsFindByFriendsWithOfferID", AnythingOfType("string"), AnythingOfType("string")).Return(offer.Deals, nil)

				req, _ := http.NewRequest("GET", fmt.Sprintf("/api/offers/%s", offer.ID), nil)
				rw = httptest.NewRecorder()
				handler.OffersGet(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("without a valid user id", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return("", errors.New("mock error"))

				req, _ := http.NewRequest("GET", fmt.Sprintf("/api/offers/%s", offer.ID), nil)
				rw = httptest.NewRecorder()
				handler.OffersGet(rw, req)
			})

			It("should return a 401 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusUnauthorized))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		// Context("with an error in CompaniesGet", func() {
		// 	BeforeEach(func() {
		// 		datastore.CompaniesGetFn = func(id int64) (*Company, error) {
		// 			return nil, errors.New("")
		// 		}
		//
		// 		req, _ = http.NewRequest("GET", fmt.Sprintf("/api/offers/%d", offerID), nil)
		//
		// 		authorizer.On("ContextWithUserID", Anything).Return(context.TODO(), nil)
		// 	})
		//
		// 	It("should return a 200 status code", func() {
		// 		Expect(rw.Code).To(Equal(http.StatusOK))
		// 	})
		//
		// 	It("should invoke datastore.OffersGet", func() {
		// 		Expect(datastore.OffersGetInvoked).To(BeTrue())
		// 	})
		//
		// 	It("should invoke datastore.CompaniesGet", func() {
		// 		Expect(datastore.CompaniesGetInvoked).To(BeTrue())
		// 	})
		//
		// 	It("should invoke datastore.DealsFindByFriendsWithOfferID", func() {
		// 		Expect(datastore.DealsFindByFriendsWithOfferIDInvoked).To(BeTrue())
		// 	})
		// })
		//
		// Context("with an error in DealsFindByFriendsWithOfferID", func() {
		// 	BeforeEach(func() {
		// 		datastore.DealsFindByFriendsWithOfferIDFn = func(userID, offerID int64) ([]*Deal, error) {
		// 			return []*Deal{}, errors.New("")
		// 		}
		//
		// 		req, _ = http.NewRequest("GET", fmt.Sprintf("/api/offers/%d", offerID), nil)
		//
		// 		authorizer.On("ContextWithUserID", Anything).Return(context.TODO(), nil)
		// 	})
		//
		// 	It("should return a 200 status code", func() {
		// 		Expect(rw.Code).To(Equal(http.StatusOK))
		// 	})
		//
		// 	It("should invoke datastore.OffersGet", func() {
		// 		Expect(datastore.OffersGetInvoked).To(BeTrue())
		// 	})
		//
		// 	It("should invoke datastore.CompaniesGet", func() {
		// 		Expect(datastore.CompaniesGetInvoked).To(BeTrue())
		// 	})
		//
		// 	It("should invoke datastore.DealsFindByFriendsWithOfferID", func() {
		// 		Expect(datastore.DealsFindByFriendsWithOfferIDInvoked).To(BeTrue())
		// 	})
		// })
	})
})
