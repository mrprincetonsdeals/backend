package handler

import (
	"bytes"
	"net/http"
	"sync"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
)

const (
	// Common log messages

	// UnauthorizedUserLogMsg is logged when access to a route is denied to an un
	UnauthorizedUserLogMsg string = "unauthorized request"
	// UnmarshallingRequestErrLogMsg is logged when unable to unmarshall a request body returned an error
	UnmarshallingRequestErrLogMsg string = "unmarshalling request body"
	// JSONMarshallingResponseErrLogMsg is logged when marshalling a json response body returned an error
	JSONMarshallingResponseErrLogMsg string = "marshalling json response body"
	// JSONAPIMarshallingResponseErrLogMsg is logged when marshalling a jsonapi response body returned an error
	JSONAPIMarshallingResponseErrLogMsg string = "marshalling jsonapi response body"
	// HTTPWriteErrLogMsg is logged when http.Write returned an error
	HTTPWriteErrLogMsg string = "writing response bytes"
	// DatastoreErrLogMsg is logged when the datastore returns an error
	DatastoreErrLogMsg string = "accessing the datastore"
	// DatastoreNotFoundLogMsg is logged when the datastore returns an error
	DatastoreNotFoundLogMsg string = "datastore returned empty response"
	// FriendCodeForSelfLogMsg is logged when a user tries to use their own invite code
	FriendCodeForSelfLogMsg string = "user tried to friend themself"
	// CreatingSessionErrLogMsg is logged when creating a jwt returns an error
	CreatingSessionErrLogMsg string = "creating session cookie"
	// PasswordMismatchErrLogMsg is logged when a user tries to log in with an incorrect password
	PasswordMismatchErrLogMsg string = "password mismatch"
	// PasswordHashErrLogMsg is logged when a user tries to log in with an incorrect password
	PasswordHashErrLogMsg string = "password hash"

	// DefaultBufferSize is the default size of the request and response buffer
	DefaultBufferSize int = 2 << 12
)

// Handler struct implements the
type Handler struct {
	DB                 Datastore
	Authorizer         Authorizer
	InviteURL          string
	Logger             zerolog.Logger
	RequestBufferPool  *sync.Pool
	ResponseBufferPool *sync.Pool
}

// New creates a new handler
func New(db Datastore, auth Authorizer, inviteURL string, ll zerolog.Logger) *Handler {
	return &Handler{
		DB:                 db,
		Authorizer:         auth,
		InviteURL:          inviteURL,
		Logger:             ll,
		RequestBufferPool:  NewBufferPool(DefaultBufferSize),
		ResponseBufferPool: NewBufferPool(DefaultBufferSize),
	}
}

// NewBufferPool creates a sync pool of a bytes buffers
func NewBufferPool(defaultSize int) *sync.Pool {
	return &sync.Pool{
		New: func() interface{} {
			buf := bytes.NewBuffer(nil)
			buf.Grow(defaultSize)
			return buf
		},
	}
}

// Router generates the routes for a handler
func (h *Handler) Router() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/health", h.HealthCheck)

	api := r.PathPrefix("/api/v1").Subrouter()
	api.Use(h.LogRequest)

	api.Handle("/codes", h.WithRequiredAuthorization(http.HandlerFunc(h.CodesCreate))).Methods("POST")
	api.Handle("/codes/{uuid}", h.WithRequiredAuthorization(http.HandlerFunc(h.CodesGet))).Methods("GET")

	api.Handle("/deals", h.WithRequiredAuthorization(http.HandlerFunc(h.DealsGetAll))).Methods("GET")
	api.Handle("/deals/{uuid}", h.WithRequiredAuthorization(http.HandlerFunc(h.DealsDelete))).Methods("DELETE")

	api.Handle("/offers", h.WithOptionalAuthorization(http.HandlerFunc(h.OffersGetRecommended))).Methods("GET")
	api.Handle("/offers/{uuid}", h.WithRequiredAuthorization(http.HandlerFunc(h.OffersGet))).Methods("GET")

	api.Handle("/companies", h.WithRequiredAuthorization(http.HandlerFunc(h.CompaniesGetAll))).Methods("GET")
	api.Handle("/companies/{uuid}", h.WithRequiredAuthorization(http.HandlerFunc(h.CompaniesGet))).Methods("GET")

	api.Handle("/friends", h.WithRequiredAuthorization(http.HandlerFunc(h.FriendsGetAll))).Methods("GET")
	api.Handle("/friends/{uuid}", h.WithRequiredAuthorization(http.HandlerFunc(h.FriendsGet))).Methods("GET")

	api.Handle("/invites", h.WithRequiredAuthorization(http.HandlerFunc(h.InviteGet))).Methods("GET")
	api.Handle("/invites/{friendCode}", h.WithRequiredAuthorization(http.HandlerFunc(h.InviteGetUser))).Methods("GET")
	api.Handle("/invites/{friendCode}", h.WithRequiredAuthorization(http.HandlerFunc(h.InviteCreateFriends))).Methods("POST")

	api.HandleFunc("/login", h.LoginAccount).Methods("POST")
	api.HandleFunc("/signup", h.CreateAccount).Methods("POST")
	api.HandleFunc("/verify", h.VerifyAccount).Methods("POST")

	// r.PathPrefix("/").Handler(http.FileServer(http.Dir(h.StaticFilesDirectory)))

	return r
}
