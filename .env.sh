#!/bin/sh

export BIND_ADDRESS=
export BIND_PORT=
export DATABASE=referral_codes_development
export DATABASE_HOST=localhost
# export DATABASE_PASSWORD=
export DATABASE_USER=$(whoami)
export DATABASE_URL="postgresql://${DATABASE_USER}:${DATABASE_PASSWORD}@${DATABASE_HOST}/${DATABASE}?sslmode=disable"
export EMAIL_SENDER="Mr. Princeton <feedback@mrprincetonsdeals.com>"
export HOSTNAME=${HOST}
export INVITE_URL=http://localhost:8080/invites
export SESSION_AUTH_KEY=
export SESSION_ENCRYPTION_KEY=
export SSL_CERT_PATH=
export SSL_KEY_PATH=
