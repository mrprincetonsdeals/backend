INSERT INTO companies (id, name, safe_name, description)
VALUES
('5b06a8c6-23df-4d42-b0c5-149e778f4e1b', 'Casper', 'casper', 'The outrageously enginiered mattress'),
('51e7059f-e179-458c-b531-77c73f6dd4f7', 'Lyft', 'lyft', 'Ride sharing company'),
('f635508e-4704-497e-8de3-116a62cde48b', 'Uber', 'uber', 'Ride sharing company');

INSERT INTO offers (id, url_prefix, company_id, description)
VALUES
('26e1a0ec-42c0-4921-b65a-9c6da7cb62d5', 'www.lyft.com/invite', '51e7059f-e179-458c-b531-77c73f6dd4f7', 'You must be new to Lyft and in an eligible market to qualify. $2.50 off your rides. You have 2 rides remaining. Offer valid until 15 days after activation. Cannot be combined with other offers. Subject to Lyft''s Terms of Service.'),
('050af187-7391-4383-a6dd-f12ef59daf97', 'www.lyft.com/drivers', '51e7059f-e179-458c-b531-77c73f6dd4f7', 'Drive towards what matters'),
('e17dd223-e033-4a03-8b1c-ad8a79f5b713', 'casper.com/friends', '5b06a8c6-23df-4d42-b0c5-149e778f4e1b', 'Purchase any size Casper plus an additional sleep product and receive $75 off your order! When you purchase, your friend will get rewarded too. Code automatically applied at checkout.');

INSERT INTO recommended_offers (offer_id, company_id)
VALUES
('26e1a0ec-42c0-4921-b65a-9c6da7cb62d5', '51e7059f-e179-458c-b531-77c73f6dd4f7'),
('050af187-7391-4383-a6dd-f12ef59daf97', '51e7059f-e179-458c-b531-77c73f6dd4f7'),
('e17dd223-e033-4a03-8b1c-ad8a79f5b713', '5b06a8c6-23df-4d42-b0c5-149e778f4e1b');

-- password: test
INSERT INTO users (id, username, email, jwt_version, salt, password, friend_code)
VALUES
('a402e20b-a902-4a24-8852-d67a02aaa9fa', 'mrp', 'mrp@example.com', 1, '\x0d5e1d73c9722f48', '\xa50dcb566102acbd479d0d7650cad571e287afef39874caf07f9e5ab0326f88e', 'code-a'),
('9352adf8-1d2c-4e61-81e3-1282c4460eed', 'posey', 'poser@example.com', 1, '\xb3624c44385d52dd', '\x8d7bcf1278510e6637052e80d6c93bbad364fdaecf002d0625fe046a2df0da6c', 'code-b');

INSERT INTO friends (user_id, friend_id)
VALUES
('a402e20b-a902-4a24-8852-d67a02aaa9fa', '9352adf8-1d2c-4e61-81e3-1282c4460eed'),
('9352adf8-1d2c-4e61-81e3-1282c4460eed', 'a402e20b-a902-4a24-8852-d67a02aaa9fa');

INSERT INTO codes(id, url)
VALUES
('1181f192-e1bb-4f06-a584-7c5c4f119269', 'www.lyft.com/invite/abctest123');

INSERT INTO deals(code_id, user_id, offer_id, company_id)
VALUES
('1181f192-e1bb-4f06-a584-7c5c4f119269', '9352adf8-1d2c-4e61-81e3-1282c4460eed', '26e1a0ec-42c0-4921-b65a-9c6da7cb62d5', '51e7059f-e179-458c-b531-77c73f6dd4f7');
