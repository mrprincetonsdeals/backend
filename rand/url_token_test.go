package rand_test

import (
	"testing"

	"gitlab.com/mrprincetonsdeals/backend/rand"
)

func BenchmarkNewFriendCode(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_, _ = rand.NewURLSafeToken()
		}
	})
}
