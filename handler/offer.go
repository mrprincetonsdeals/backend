package handler

import (
	"bytes"
	"net/http"

	"github.com/google/jsonapi"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/hlog"
)

// OffersGetRecommended route that retrieves all offers for the landing page
func (h *Handler) OffersGetRecommended(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	offers, err := h.DB.OffersGetRecommended()
	if err != nil {
		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, offers)
	if err != nil {
		ll.Error().Err(err).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/vnd.api+json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}
}

// OffersGet route that retrieves an offer by its uuid
func (h *Handler) OffersGet(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	userID, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Error().Err(err).Msg(UnauthorizedUserLogMsg)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	vars := mux.Vars(r)
	offerID := vars["uuid"]

	offer, err := h.DB.OffersGet(offerID)
	if err != nil {
		ll.Error().Err(err).Str("offer_id", offerID).Msg(DatastoreErrLogMsg)
		http.Error(w, "Not Found", http.StatusNoContent)
		return
	}

	company, err := h.DB.CompaniesGet(offer.CompanyID)
	if err == nil {
		offer.Company = company
	}

	deals, err := h.DB.DealsFindByFriendsWithOfferID(userID, offerID)
	if err == nil {
		offer.Deals = deals
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, offer)
	if err != nil {
		ll.Error().Err(err).Str("offer_id", offerID).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/vnd.api+json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}
}
