package postgres

import (
	"github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

// FriendsCount retrieves the total number of friends for the user's uuid in the database
func (db *Datastore) FriendsCount(userID string) (int, error) {
	var count int
	err := db.DB.QueryRow("SELECT COUNT(*) FROM friends WHERE user_id = $1;", userID).Scan(&count)
	if err != nil {
		return 0, err
	}
	return count, nil
}

// FriendsCreate creates a new friend for the current user
func (db *Datastore) FriendsCreate(userID string, friendID string) error {
	rows, err := db.DB.Query(`SELECT id FROM users WHERE id = $1;`, friendID)
	if err != nil {
		return err
	}

	txn, err := db.DB.Begin()
	if err != nil {
		return err
	}

	for rows.Next() {
		var friendID string

		if err = rows.Scan(&friendID); err != nil {
			return err
		}

		_, err = txn.Exec("INSERT INTO friends(user_id, friend_id) VALUES ($1, $2), ($2, $1) ON CONFLICT (user_id, friend_id) DO NOTHING", userID, friendID)
		if err != nil {
			return err
		}
	}

	err = txn.Commit()
	if err != nil {
		return err
	}

	return nil
}

// FriendsGetAll retrieves all friends for a user by uuid, paged
func (db *Datastore) FriendsGetAll(userID string, limit, offset int) ([]interface{}, error) {
	rows, err := db.DB.Query("SELECT friend_id, u.username FROM friends INNER JOIN users u on friend_id = u.id WHERE user_id = $1 LIMIT $2 OFFSET $3;", userID, limit, offset)
	if err != nil {
		return nil, err
	}

	var (
		results   []interface{}
		friendIDs []string
	)
	for rows.Next() {
		var friend models.Friend
		if err := rows.Scan(&friend.ID, &friend.Name); err != nil {
			return nil, err
		}
		results = append(results, &friend)
		friendIDs = append(friendIDs, friend.ID)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	rows, err = db.DB.Query(`SELECT user_id, count(*) FROM deals WHERE is_deleted = false AND company_id IS NOT NULL AND user_id = ANY($1) GROUP BY user_id;`, pq.Array(friendIDs))
	if err != nil {
		return nil, err
	}

	friendDeals := map[string]int{}
	for rows.Next() {
		var (
			friendID  string
			dealCount int
		)

		if err := rows.Scan(&friendID, &dealCount); err != nil {
			return nil, err
		}

		friendDeals[friendID] = dealCount
	}

	for _, friendInterface := range results {
		friend, ok := friendInterface.(*models.Friend)
		if !ok {
			continue
		}

		count, ok := friendDeals[friend.ID]
		if ok {
			friend.DealCount = count
		}
	}

	return results, nil
}

// FriendsGet retrieves a single friend by their uuid
func (db *Datastore) FriendsGet(userID, friendID string) (*models.Friend, error) {
	var result bool
	err := db.DB.QueryRow("SELECT EXISTS (SELECT 1 FROM friends WHERE user_id = $1 AND friend_id = $2);", userID, friendID).Scan(&result)
	if err != nil {
		return nil, err
	}

	if !result {
		return nil, nil
	}

	friend := &models.Friend{}

	row := db.DB.QueryRow("SELECT id, username FROM users WHERE id = $1", friendID)
	err = row.Scan(&friend.ID, &friend.Name)
	if err != nil {
		return nil, err
	}

	rows, err := db.DB.Query(`SELECT id, code_id, offer_id, company_id FROM deals WHERE user_id = $1 AND is_deleted = false AND offer_id IS NOT NULL AND company_id IS NOT NULL;`, friend.ID)
	if err != nil {
		return nil, err
	}

	var (
		offerIDs   []string
		companyIDs []string
	)

	for rows.Next() {
		deal := &models.Deal{}

		if err = rows.Scan(&deal.ID, &deal.CodeID, &deal.OfferID, &deal.CompanyID); err != nil {
			return nil, err
		}

		friend.Deals = append(friend.Deals, deal)
		offerIDs = append(offerIDs, deal.OfferID)
		companyIDs = append(companyIDs, deal.CompanyID)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	offers := map[string]*models.Offer{}
	rows, err = db.DB.Query(`SELECT id, description FROM offers WHERE id = ANY($1);`, pq.Array(offerIDs))
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var offer models.Offer
		if err := rows.Scan(&offer.ID, &offer.Description); err != nil {
			return nil, err
		}

		offers[offer.ID] = &offer
	}

	if err := rows.Err(); err != nil {
		return friend, err
	}

	companies := map[string]*models.Company{}
	rows, err = db.DB.Query(`SELECT id, name FROM companies WHERE id = ANY($1);`, pq.Array(companyIDs))
	for rows.Next() {
		var company models.Company
		if err := rows.Scan(&company.ID, &company.Name); err != nil {
			return nil, err
		}

		companies[company.ID] = &company
	}

	if err := rows.Err(); err != nil {
		return friend, err
	}

	for _, deal := range friend.Deals {
		offer, ok := offers[deal.OfferID]
		if ok {
			deal.Offer = offer
		}

		company, ok := companies[deal.CompanyID]
		if ok {
			deal.Company = company
		}
	}

	return friend, err
}

// CreateFriendCode generates a new invite code for a user and stores it on the db.friendCodesMap
func (db *Datastore) CreateFriendCode() (string, error) {
	var (
		stored = false
		code   string
		err    error
	)

	for !stored {
		code, err = db.FriendCodeCreator()
		if err != nil {
			return "", errors.Wrap(err, "friend code create")
		}

		_, loaded := db.friendCodes.LoadOrStore(code, struct{}{})
		stored = !loaded
	}

	return code, nil
}

// UpdateFriendCodesMap retrieves all friend codes from the database
func (db *Datastore) UpdateFriendCodesMap() error {
	query := "SELECT friend_code FROM users;"

	rows, err := db.DB.Query(query)
	if err != nil {
		return err
	}

	for rows.Next() {
		var code string
		if err := rows.Scan(&code); err != nil {
			return err
		}

		db.friendCodes.Store(code, struct{}{})
	}

	if err := rows.Err(); err != nil {
		return err
	}

	return nil
}
