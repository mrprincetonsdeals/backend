package handler

var (
	// Error payloads returned to the frontend

	// PasswordMismatchResp returned when the verify fails because the password and password-verify don't match
	PasswordMismatchResp = []byte(`{"errors": ["errors.passwordMismatch"]}`)

	// LoginFailureResp returned when the login fails because the email address or password is invalid
	LoginFailureResp = []byte(`{"errors": ["errors.loginFailure"]}`)

	// InvalidPayloadResp returned when the request body is unparseable
	InvalidPayloadResp = []byte(`{"errors": ["errors.invalidReq"]}`)

	// InvalidTokenResp returned when a token has expired or is incorrect
	InvalidTokenResp = []byte(`{"errors": ["errors.invalidToken"]}`)

	// FriendCodeForSelfResp returned when a user visits their own invite code
	FriendCodeForSelfResp = []byte(`{"errors": ["errors.friendCodeSelf"]}`)
)
