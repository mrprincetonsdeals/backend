package handler_test

import (
	"bytes"
	"errors"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strconv"

	"github.com/google/jsonapi"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/rs/zerolog"
	. "github.com/stretchr/testify/mock"
	. "gitlab.com/mrprincetonsdeals/backend/handler"
	"gitlab.com/mrprincetonsdeals/backend/mocks"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var _ = Describe("Code Helper Methods", func() {
	Describe("CodeIsValid", func() {
		// It("should return an error with an empty LinkToDeal", func() {
		// 	err := DealIsValid(Deal{})
		// 	Expect(err).To(HaveOccurred())
		// 	Expect(err).To(Equal(InvalidLinkError))
		// })

		// It("should return an error if LinkToDeal can't be parsed as a valid URL", func() {
		// 	err := DealIsValid(Deal{LinkToDeal: "%"})
		// 	Expect(err).To(HaveOccurred())
		// })
	})

})

var _ = Describe("Code Handler", func() {
	var (
		handler    *Handler
		userID     string
		datastore  *mocks.Datastore
		authorizer *mocks.Authorizer
		writer     *mocks.Writer
		rw         *httptest.ResponseRecorder
	)

	BeforeEach(func() {
		userID = strconv.FormatInt(rand.Int63(), 10)
		datastore = &mocks.Datastore{}
		authorizer = &mocks.Authorizer{}
		writer = &mocks.Writer{}
		writer.On("Write", Anything).Return(0, nil)
		logger := zerolog.New(writer)
		inviteURL := "http://example.com/friends/new?invite="

		handler = New(datastore, authorizer, inviteURL, logger)
	})

	Describe("CodesCreate", func() {
		var (
			newCode *models.Code
		)

		BeforeEach(func() {
			newCode = mocks.NewCode()
		})

		Context("with a valid user id", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return(userID, nil)
				datastore.On("CodesCreate", Anything, Anything).Return(newCode, nil)

				payload := bytes.NewBuffer(nil)
				err := jsonapi.MarshalPayload(payload, newCode)
				Expect(err).NotTo(HaveOccurred())

				req, _ := http.NewRequest("POST", "/api/codes", payload)
				rw = httptest.NewRecorder()
				handler.CodesCreate(rw, req)
			})

			It("should return a 201 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusCreated))
			})

			It("should return the new code as json", func() {
				buf := bytes.NewBuffer(nil)
				err := jsonapi.MarshalPayload(buf, newCode)
				Expect(err).NotTo(HaveOccurred())

				Expect(rw.Body).To(MatchJSON(buf))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("when the database write fails", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return(userID, nil)
				datastore.On("CodesCreate", Anything, Anything).Return(nil, errors.New("mock error"))

				payload := bytes.NewBuffer(nil)
				err := jsonapi.MarshalPayload(payload, newCode)
				Expect(err).NotTo(HaveOccurred())

				req, _ := http.NewRequest("POST", "/api/codes", payload)
				rw = httptest.NewRecorder()
				handler.CodesCreate(rw, req)
			})

			It("should return a 400 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusBadRequest))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("with an invalid payload", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return(userID, nil)

				req, _ := http.NewRequest("POST", "/api/codes", bytes.NewBuffer([]byte("")))
				rw = httptest.NewRecorder()
				handler.CodesCreate(rw, req)
			})

			It("should return a 201 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusBadRequest))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})

			// It("should write an error message to the logs", func() {
			//
			// })
		})

		Context("with out a valid user id", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return("", errors.New("mock error"))

				req, _ := http.NewRequest("POST", "/api/codes", nil)
				rw = httptest.NewRecorder()
				handler.CodesCreate(rw, req)
			})

			It("should return a 401 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusUnauthorized))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})
	})

	Describe("CodesGet", func() {
		var (
			code *models.Code
		)

		BeforeEach(func() {
			code = mocks.NewCode()
		})

		Context("with a valid user id", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return(userID, nil)
				datastore.On("CodesGet", Anything).Return(code, nil)

				req, _ := http.NewRequest("POST", "/api/codes", nil)
				rw = httptest.NewRecorder()
				handler.CodesGet(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should return the code as json", func() {
				buf := bytes.NewBuffer(nil)
				err := jsonapi.MarshalPayload(buf, code)
				Expect(err).NotTo(HaveOccurred())

				Expect(rw.Body).To(MatchJSON(buf))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("when the database write fails", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return(userID, nil)
				datastore.On("CodesGet", Anything).Return(nil, errors.New("mock error"))

				req, _ := http.NewRequest("POST", "/api/codes", nil)
				rw = httptest.NewRecorder()
				handler.CodesGet(rw, req)
			})

			It("should return a 404 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusNoContent))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("with out a valid user id", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return("", errors.New("mock error"))

				req, _ := http.NewRequest("GET", "/api/codes", nil)
				rw = httptest.NewRecorder()
				handler.CodesGet(rw, req)
			})

			It("should return a 401 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusUnauthorized))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})
	})
})
