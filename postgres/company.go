package postgres

import (
	"github.com/lib/pq"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

// CompaniesCount retrieves the total number of companies in the database
func (db *Datastore) CompaniesCount() (int, error) {
	var count int
	err := db.DB.QueryRow("SELECT COUNT(*) FROM companies;").Scan(&count)
	if err != nil {
		return 0, err
	}
	return count, nil
}

// CompaniesGetAll retrieves all companies, paged
func (db *Datastore) CompaniesGetAll(limit, offset int) ([]interface{}, error) {
	rows, err := db.DB.Query("SELECT id, name, description FROM companies LIMIT $1 OFFSET $2;", limit, offset)
	if err != nil {
		return nil, err
	}

	var (
		results    []interface{}
		companyIDs []string
	)

	for rows.Next() {
		var company models.Company
		if err := rows.Scan(&company.ID, &company.Name, &company.Description); err != nil {
			return nil, err
		}
		company.Offers = []*models.Offer{}

		results = append(results, &company)
		companyIDs = append(companyIDs, company.ID)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	rows, err = db.DB.Query(`SELECT id, description, company_id FROM offers WHERE company_id = ANY($1);`, pq.Array(companyIDs))
	if err != nil {
		return nil, err
	}

	companyOffers := map[string][]*models.Offer{}
	for rows.Next() {
		var offer models.Offer

		if err := rows.Scan(&offer.ID, &offer.Description, &offer.CompanyID); err != nil {
			return nil, err
		}

		offers, ok := companyOffers[offer.CompanyID]

		if ok {
			companyOffers[offer.CompanyID] = append(offers, &offer)
		} else {
			companyOffers[offer.CompanyID] = []*models.Offer{&offer}
		}
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	for _, companyInterface := range results {
		company, ok := companyInterface.(*models.Company)
		if !ok {
			continue
		}

		offer, ok := companyOffers[company.ID]
		if !ok {
			continue
		}
		company.Offers = offer
	}

	return results, nil
}

// CompaniesGet retrieves a single company by its uuid
func (db *Datastore) CompaniesGet(id string) (*models.Company, error) {
	row := db.DB.QueryRow("SELECT id, name, description FROM companies WHERE id = $1", id)

	var company models.Company
	company.Offers = []*models.Offer{}

	err := row.Scan(&company.ID, &company.Name, &company.Description)
	if err != nil {
		return nil, err
	}

	rows, err := db.DB.Query(`SELECT id, description, company_id FROM offers WHERE company_id = $1;`, id)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var offer models.Offer

		if err := rows.Scan(&offer.ID, &offer.Description, &offer.CompanyID); err != nil {
			return nil, err
		}

		company.Offers = append(company.Offers, &offer)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return &company, nil
}
