FROM golang:1.12.6-alpine@sha256:c750d6718009f2e94cb20f56a87884f601f175d43c9418ae0fa21ea00ad6a2ff AS builder

RUN apk --no-cache add ca-certificates git

WORKDIR /codes

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o referral-codes-web-app ./cmd/referral-codes-web-app

FROM alpine:3.10.0@sha256:ca1c944a4f8486a153024d9965aafbe24f5723c1d5c02f4964c045a16d19dc54

RUN apk --no-cache add ca-certificates

WORKDIR /app/

COPY --from=builder /codes/referral-codes-web-app .

CMD ["/app/referral-codes-web-app"]