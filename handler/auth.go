package handler

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"io"
	"net/http"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/hlog"
	"gitlab.com/mrprincetonsdeals/backend/models"
	password "gitlab.com/mrprincetonsdeals/backend/password/v1"
	"gitlab.com/mrprincetonsdeals/backend/postgres"
)

// CreateAccountReq struct that contains the create account payload
type CreateAccountReq struct {
	Email          string `json:"email"`
	Username       string `json:"username"`
	Password       string `json:"password"`
	VerifyPassword string `json:"verifyPassword"`
}

// CreateAccount route that creates an account and sends an email to verify the email address
func (h *Handler) CreateAccount(w http.ResponseWriter, r *http.Request) {
	buf := h.RequestBufferPool.Get().(*bytes.Buffer)
	defer h.RequestBufferPool.Put(buf)
	buf.Reset()

	ll := hlog.FromRequest(r)

	_, err := io.CopyN(buf, r.Body, r.ContentLength)
	if err != nil {
		ll.Error().Err(err).Msg(UnmarshallingRequestErrLogMsg)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	req := &CreateAccountReq{}
	err = json.Unmarshal(buf.Bytes(), req)
	if err != nil {
		ll.Error().Err(err).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	if req.Password != req.VerifyPassword {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		w.Write(PasswordMismatchResp)
		return
	}

	salt, _ := password.GenerateSalt()
	pass := []byte(req.Password)
	hashPass, _ := password.Hash(pass, salt)

	user := &models.User{
		Email:          req.Email,
		Username:       req.Username,
		Salt:           salt,
		HashedPassword: hashPass,
	}

	verifyToken, err := h.DB.UsersCreate(user)
	if err != nil {
		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	// Email user their token
	ll.Info().Str("token", verifyToken).Msg("created unverified user")

	w.WriteHeader(http.StatusNoContent)
}

// VerifyAccountReq struct that contains the verify account payload
type VerifyAccountReq struct {
	VerifyToken string `json:"verifyToken"`
}

// VerifyAccount route that verifies the email exists and creates the account
func (h *Handler) VerifyAccount(w http.ResponseWriter, r *http.Request) {
	buf := h.RequestBufferPool.Get().(*bytes.Buffer)
	defer h.RequestBufferPool.Put(buf)
	buf.Reset()

	ll := hlog.FromRequest(r)

	_, err := io.CopyN(buf, r.Body, r.ContentLength)
	if err != nil {
		ll.Error().Err(err).Msg(UnmarshallingRequestErrLogMsg)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	req := &VerifyAccountReq{}
	err = json.Unmarshal(buf.Bytes(), req)
	if err != nil {
		ll.Error().Err(err).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	err = h.DB.UsersVerify(req.VerifyToken)
	if err != nil {
		cause := errors.Cause(err)
		if cause == postgres.ErrUnknownVerify || cause == sql.ErrNoRows {
			ll.Error().Err(err).Msg(DatastoreErrLogMsg)
			w.WriteHeader(http.StatusBadRequest)
			w.Header().Set("Content-Type", "application/json")
			w.Write(InvalidTokenResp)
			return
		}

		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// LoginAccountReq struct that contains the login account payload
type LoginAccountReq struct {
	Email      string `json:"email"`
	Password   string `json:"password"`
	FriendCode string `json:"friendCode"`
}

// LoginAccount route that sets the authenticates the user and sets the login cookie
func (h *Handler) LoginAccount(w http.ResponseWriter, r *http.Request) {
	buf := h.RequestBufferPool.Get().(*bytes.Buffer)
	defer h.RequestBufferPool.Put(buf)
	buf.Reset()

	ll := hlog.FromRequest(r)

	_, err := io.CopyN(buf, r.Body, r.ContentLength)
	if err != nil {
		ll.Error().Err(err).Msg(UnmarshallingRequestErrLogMsg)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	req := &LoginAccountReq{}
	err = json.Unmarshal(buf.Bytes(), req)
	if err != nil {
		ll.Error().Err(err).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	user, err := h.DB.UsersGet(req.Email)
	if err != nil {
		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	salt := []byte{}
	dbHash := []byte{}
	if user != nil {
		salt = user.Salt
		dbHash = user.HashedPassword
	}

	pass := []byte(req.Password)
	hashedPass, _ := password.Hash(pass, salt)

	verified := password.ComparePasswordToHash(hashedPass, dbHash)

	if user == nil || !verified {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json")
		w.Write(LoginFailureResp)
		return
	}

	friendCode := req.FriendCode
	if friendCode != "" {
		userID := user.ID

		newFriend, err := h.DB.UsersGetByFriendCode(friendCode)
		if err != nil {
			ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		}

		if newFriend == nil {
			ll.Error().Err(err).Str("friend_code", friendCode).Msg(DatastoreErrLogMsg)
		}

		friendID := newFriend.ID

		err = h.DB.FriendsCreate(userID, friendID)
		if err != nil {
			ll.Error().Err(err).Str("user_id", userID).Str("friend_id", friendID).Msg(DatastoreErrLogMsg)
		}
	}

	err = h.Authorizer.AddAuthCookieToRequest(w, r, user.ID)
	if err != nil {
		ll.Error().Err(err).Msg(CreatingSessionErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// WithRequiredAuthorization adds the user id to the context or returns a 401 Unauthorizedd
func (h *Handler) WithRequiredAuthorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ll := hlog.FromRequest(r)

		ctx, err := h.Authorizer.ContextWithUserID(r.Context(), r)
		if err != nil {
			ll.Error().Err(err).Msg(UnauthorizedUserLogMsg)
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
		} else {
			next.ServeHTTP(w, r.WithContext(ctx))
		}
	})
}

// WithOptionalAuthorization adds the user id to the context if user is authorized
func (h *Handler) WithOptionalAuthorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, _ := h.Authorizer.ContextWithUserID(r.Context(), r)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
