.PHONY: all build install lint migrate-production migrate-development run race start-local stop-local test update-deps update-mocks

git_sha := $(shell git rev-parse --short HEAD)
ld_flags := -ldflags "-X 'main.GitSHA=$(git_sha)'"
build_cmd := go build $(ld_flags) -o bin/referral-codes-web-app ./cmd/referral-codes-web-app

all: build test install

# Build

bench:
	go test -bench=. -cpu=1,4,16 -benchmem

build:
	$(build_cmd)

build-linux:
	CGO_ENABLED=0 GOOS=linux $(build_cmd)

install:
	go install ./...

lint:
	golangci-lint run

run:
	go run $(ld_flags) ./cmd/referral-codes-web-app

race:
	go test -race ./...

test:
	go test -cover ./...

update:
	go get -u all

update-deps:
	go get -u all

update-mocks:
	mockery -all -dir handler

# Migrate

migrate-production:
	./scripts/shmig.sh -t postgresql -H ${DATABASE_HOST} -l ${DATABASE_USER} -p ${DATABASE_PASSWORD} -d ${DATABASE} up

migrate-development:
	./scripts/shmig.sh -t postgresql -H ${DATABASE_HOST} -l ${DATABASE_USER} -p ${DATABASE_PASSWORD} -d ${DATABASE} up

migrate-development-down:
	./scripts/shmig.sh -t postgresql -H ${DATABASE_HOST} -l ${DATABASE_USER} -p ${DATABASE_PASSWORD} -d ${DATABASE} down

psql-development:
	PGPASSWORD=${DATABASE_PASSWORD} psql -h ${DATABASE_HOST} -p 5432 ${DATABASE}

seed-database:
	PGPASSWORD=${DATABASE_PASSWORD} psql -h ${DATABASE_HOST} -p 5432 ${DATABASE} < testdata/seeds.sql

# Docker

start-local:
	docker-compose up --build -d

stop-local:
	docker-compose down
