package handler_test

import (
	"errors"
	"net/http"
	"net/http/httptest"

	"github.com/gorilla/mux"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/rs/zerolog"
	. "github.com/stretchr/testify/mock"
	. "gitlab.com/mrprincetonsdeals/backend/handler"
	"gitlab.com/mrprincetonsdeals/backend/mocks"
)

var _ = Describe("Health Check Handler", func() {
	var (
		datastore  *mocks.Datastore
		authorizer *mocks.Authorizer
		writer     *mocks.Writer
		mux        *mux.Router
		req        *http.Request
		rw         *httptest.ResponseRecorder
	)

	BeforeEach(func() {
		datastore = &mocks.Datastore{}
		authorizer = &mocks.Authorizer{}
		writer = &mocks.Writer{}
		writer.On("Write", Anything).Return(0, nil)
		logger := zerolog.New(writer)
		inviteURL := "http://example.com/friends/new?invite="

		handler := New(datastore, authorizer, inviteURL, logger)
		mux = handler.Router()
	})

	JustBeforeEach(func() {
		rw = httptest.NewRecorder()
		mux.ServeHTTP(rw, req)
	})

	Context("", func() {
		BeforeEach(func() {
			datastore.On("Ping").Return(nil)

			req, _ = http.NewRequest("GET", "/health", nil)
		})

		It("should return a 200 status code", func() {
			Expect(rw.Code).To(Equal(http.StatusOK))
		})

		It("should invoke expected authorizer methods", func() {
			authorizer.AssertExpectations(GinkgoT())
		})

		It("should invoke expected datastore methods", func() {
			datastore.AssertExpectations(GinkgoT())
		})
	})

	Context("with an error", func() {
		BeforeEach(func() {
			datastore.On("Ping").Return(errors.New("mock error"))

			req, _ = http.NewRequest("GET", "/health", nil)
		})

		It("should return a 500 status code", func() {
			Expect(rw.Code).To(Equal(http.StatusInternalServerError))
		})

		It("should invoke expected authorizer methods", func() {
			authorizer.AssertExpectations(GinkgoT())
		})

		It("should invoke expected datastore methods", func() {
			datastore.AssertExpectations(GinkgoT())
		})
	})
})
