[![build status](https://gitlab.com/mrprincetonsdeals/backend/badges/master/build.svg)](https://gitlab.com/mrprincetonsdeals/backend/commits/master) [![coverage report](https://gitlab.com/mrprincetonsdeals/backend/badges/master/coverage.svg)](https://gitlab.com/mrprincetonsdeals/backend/commits/master)

# Referral Codes Backend

## Dependencies

## Glossary

company - The organization offering referral codes.
offer - The details of a referral code, what a company is offering. A company can have multiple offers.
deal - The link between an offer and a code and a user.
code - The url for a specific user's referral code.
friend - A user who has mutual access with your deals. This might change to be a follow relationship.
friendCode - The url safe character code to add someone as your friend.

## Run database migrations

```sh
make migrate-development
```

## Create a new database migration

```sh
./scripts/shmig.sh -t postgresql -d $DATABASE create $MIGRATION_FILENAME
```

## Seed the database

```sh
psql $DATABASE_URL < testdata/seeds.sql
```

## Running benchmarks

```sh
make bench
```

## Running a gitlab build

```sh
gitlab-runner exec docker test
```

## If you ever wanted a slow database test

```
BeforeEach(func() {
  dbName = "companies_test"
  cmd := exec.Command("createdb", dbName)
  cmd.Run()

  dbURL := fmt.Sprintf("postgres://localhost/%s?sslmode=disable", dbName)
  db, _ := sql.Open("postgres", dbURL)

  goose.Up(db, "db/migrations")

  seed, _ := ioutil.ReadFile("testdata/company_seeds.sql")
  db.Query(string(seed))

  datastore = &DB{db}
})

AfterEach(func() {
  cmd := exec.Command("dropdb", dbName)
  cmd.Run()
})
```
