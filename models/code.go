package models

// Code is the url for a referral code
type Code struct {
	ID   string `jsonapi:"primary,codes"`
	URL  string `jsonapi:"attr,url"`
	Deal *Deal  `jsonapi:"relation,deal,omitempty"`
}
