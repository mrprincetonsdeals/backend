package models

// Invite is the model for a user's invite code
type Invite struct {
	ID        string `jsonapi:"primary,invites"`
	InviteURL string `jsonapi:"attr,invite-url"`
}
