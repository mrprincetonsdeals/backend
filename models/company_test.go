package models_test

import (
	"bytes"
	"fmt"

	"github.com/google/jsonapi"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var _ = Describe("Offer", func() {
	var (
		buf         *bytes.Buffer
		err         error
		companyID   string
		name        string
		description string
		logoURL     string
	)

	BeforeEach(func() {
		companyID = uuid.New().String()
		name = "Company Name"
		description = "Company Description"
		logoURL = "https://test/gif.png"
		company := &models.Company{
			ID:          companyID,
			Name:        name,
			Description: description,
			LogoURL:     logoURL,
		}

		buf = bytes.NewBuffer(nil)
		err = jsonapi.MarshalPayload(buf, company)
	})

	It("should not have any errors", func() {
		Expect(err).NotTo(HaveOccurred())
	})

	It("should have the correct type", func() {
		Expect(buf.String()).To(ContainSubstring(`"type":"companies"`))
	})

	It("should have the correct id", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"id":"%s"`, companyID)))
	})

	It("should have the correct attributes", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"attributes":{"description":"%s","logo-url":"%s","name":"%s","safe-name":""}`, description, logoURL, name)))
	})
})
