package models_test

import (
	"bytes"
	"fmt"
	"math/rand"

	"github.com/google/jsonapi"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var _ = Describe("Friend", func() {
	var (
		buf       *bytes.Buffer
		err       error
		friendID  string
		name      string
		dealCount int
	)

	BeforeEach(func() {
		friendID = uuid.New().String()
		name = "Friend's name"
		dealCount = rand.Int()
		friend := &models.Friend{
			ID:        friendID,
			Name:      name,
			DealCount: dealCount,
		}

		buf = bytes.NewBuffer(nil)
		err = jsonapi.MarshalPayload(buf, friend)
	})

	It("should not have any errors", func() {
		Expect(err).NotTo(HaveOccurred())
	})

	It("should have the correct type", func() {
		Expect(buf.String()).To(ContainSubstring(`"type":"friends"`))
	})

	It("should have the correct id", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"id":"%s"`, friendID)))
	})

	It("should have the correct attributes", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"attributes":{"deal-count":%d,"name":"%s"}`, dealCount, name)))
	})
})
