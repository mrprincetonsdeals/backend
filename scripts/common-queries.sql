
-- Select all codes with each user's name
SELECT d.id as deal_id, c.url, u.name
  FROM codes c
  INNER JOIN deals d on d.code_id = c.id
  INNER JOIN users u on u.id = d.user_id;

-- Create a new company and associate it with offers
INSERT INTO companies (name, safe_name, description)
  VALUES
  ('', '', '');

INSERT INTO offers (url_prefix, company_id, description)
  VALUES('', '', '');

UPDATE deals
  SET offer_id = '',
    company_id = ''
  WHERE id = '';
