// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	models "gitlab.com/mrprincetonsdeals/backend/models"
)

// Datastore is an autogenerated mock type for the Datastore type
type Datastore struct {
	mock.Mock
}

// CodesCreate provides a mock function with given fields: userID, url
func (_m *Datastore) CodesCreate(userID string, url string) (*models.Code, error) {
	ret := _m.Called(userID, url)

	var r0 *models.Code
	if rf, ok := ret.Get(0).(func(string, string) *models.Code); ok {
		r0 = rf(userID, url)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Code)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(userID, url)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CodesGet provides a mock function with given fields: id
func (_m *Datastore) CodesGet(id string) (*models.Code, error) {
	ret := _m.Called(id)

	var r0 *models.Code
	if rf, ok := ret.Get(0).(func(string) *models.Code); ok {
		r0 = rf(id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Code)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CompaniesCount provides a mock function with given fields:
func (_m *Datastore) CompaniesCount() (int, error) {
	ret := _m.Called()

	var r0 int
	if rf, ok := ret.Get(0).(func() int); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(int)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CompaniesGet provides a mock function with given fields: id
func (_m *Datastore) CompaniesGet(id string) (*models.Company, error) {
	ret := _m.Called(id)

	var r0 *models.Company
	if rf, ok := ret.Get(0).(func(string) *models.Company); ok {
		r0 = rf(id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Company)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// CompaniesGetAll provides a mock function with given fields: limit, offset
func (_m *Datastore) CompaniesGetAll(limit int, offset int) ([]interface{}, error) {
	ret := _m.Called(limit, offset)

	var r0 []interface{}
	if rf, ok := ret.Get(0).(func(int, int) []interface{}); ok {
		r0 = rf(limit, offset)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]interface{})
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int, int) error); ok {
		r1 = rf(limit, offset)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// DealDelete provides a mock function with given fields: userID, dealID
func (_m *Datastore) DealDelete(userID string, dealID string) error {
	ret := _m.Called(userID, dealID)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, string) error); ok {
		r0 = rf(userID, dealID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DealsFindByFriendsWithOfferID provides a mock function with given fields: userID, offerID
func (_m *Datastore) DealsFindByFriendsWithOfferID(userID string, offerID string) ([]*models.Deal, error) {
	ret := _m.Called(userID, offerID)

	var r0 []*models.Deal
	if rf, ok := ret.Get(0).(func(string, string) []*models.Deal); ok {
		r0 = rf(userID, offerID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*models.Deal)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(userID, offerID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// DealsFindByUserID provides a mock function with given fields: userID
func (_m *Datastore) DealsFindByUserID(userID string) ([]interface{}, error) {
	ret := _m.Called(userID)

	var r0 []interface{}
	if rf, ok := ret.Get(0).(func(string) []interface{}); ok {
		r0 = rf(userID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]interface{})
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// FriendsCount provides a mock function with given fields: userID
func (_m *Datastore) FriendsCount(userID string) (int, error) {
	ret := _m.Called(userID)

	var r0 int
	if rf, ok := ret.Get(0).(func(string) int); ok {
		r0 = rf(userID)
	} else {
		r0 = ret.Get(0).(int)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// FriendsCreate provides a mock function with given fields: userID, friendID
func (_m *Datastore) FriendsCreate(userID string, friendID string) error {
	ret := _m.Called(userID, friendID)

	var r0 error
	if rf, ok := ret.Get(0).(func(string, string) error); ok {
		r0 = rf(userID, friendID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// FriendsGet provides a mock function with given fields: userID, friendID
func (_m *Datastore) FriendsGet(userID string, friendID string) (*models.Friend, error) {
	ret := _m.Called(userID, friendID)

	var r0 *models.Friend
	if rf, ok := ret.Get(0).(func(string, string) *models.Friend); ok {
		r0 = rf(userID, friendID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Friend)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(userID, friendID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// FriendsGetAll provides a mock function with given fields: userID, limit, offset
func (_m *Datastore) FriendsGetAll(userID string, limit int, offset int) ([]interface{}, error) {
	ret := _m.Called(userID, limit, offset)

	var r0 []interface{}
	if rf, ok := ret.Get(0).(func(string, int, int) []interface{}); ok {
		r0 = rf(userID, limit, offset)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]interface{})
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, int, int) error); ok {
		r1 = rf(userID, limit, offset)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// OffersGet provides a mock function with given fields: id
func (_m *Datastore) OffersGet(id string) (*models.Offer, error) {
	ret := _m.Called(id)

	var r0 *models.Offer
	if rf, ok := ret.Get(0).(func(string) *models.Offer); ok {
		r0 = rf(id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Offer)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// OffersGetRecommended provides a mock function with given fields:
func (_m *Datastore) OffersGetRecommended() ([]*models.Offer, error) {
	ret := _m.Called()

	var r0 []*models.Offer
	if rf, ok := ret.Get(0).(func() []*models.Offer); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*models.Offer)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Ping provides a mock function with given fields:
func (_m *Datastore) Ping() error {
	ret := _m.Called()

	var r0 error
	if rf, ok := ret.Get(0).(func() error); ok {
		r0 = rf()
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UsersCreate provides a mock function with given fields: user
func (_m *Datastore) UsersCreate(user *models.User) (string, error) {
	ret := _m.Called(user)

	var r0 string
	if rf, ok := ret.Get(0).(func(*models.User) string); ok {
		r0 = rf(user)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*models.User) error); ok {
		r1 = rf(user)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UsersGet provides a mock function with given fields: email
func (_m *Datastore) UsersGet(email string) (*models.User, error) {
	ret := _m.Called(email)

	var r0 *models.User
	if rf, ok := ret.Get(0).(func(string) *models.User); ok {
		r0 = rf(email)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.User)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(email)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UsersGetByFriendCode provides a mock function with given fields: friendCode
func (_m *Datastore) UsersGetByFriendCode(friendCode string) (*models.User, error) {
	ret := _m.Called(friendCode)

	var r0 *models.User
	if rf, ok := ret.Get(0).(func(string) *models.User); ok {
		r0 = rf(friendCode)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.User)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(friendCode)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UsersGetFriendCode provides a mock function with given fields: userID
func (_m *Datastore) UsersGetFriendCode(userID string) (string, error) {
	ret := _m.Called(userID)

	var r0 string
	if rf, ok := ret.Get(0).(func(string) string); ok {
		r0 = rf(userID)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UsersVerify provides a mock function with given fields: verifyToken
func (_m *Datastore) UsersVerify(verifyToken string) error {
	ret := _m.Called(verifyToken)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(verifyToken)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
