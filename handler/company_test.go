package handler_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"

	"github.com/google/jsonapi"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/rs/zerolog"
	. "github.com/stretchr/testify/mock"
	. "gitlab.com/mrprincetonsdeals/backend/handler"
	"gitlab.com/mrprincetonsdeals/backend/mocks"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var _ = Describe("Company Handler", func() {
	var (
		handler    *Handler
		datastore  *mocks.Datastore
		authorizer *mocks.Authorizer
		writer     *mocks.Writer
		rw         *httptest.ResponseRecorder
	)

	BeforeEach(func() {
		datastore = &mocks.Datastore{}
		authorizer = &mocks.Authorizer{}
		writer = &mocks.Writer{}
		writer.On("Write", Anything).Return(0, nil)
		logger := zerolog.New(writer)
		inviteURL := "http://example.com/friends/new?invite="

		handler = New(datastore, authorizer, inviteURL, logger)
	})

	Describe("CompaniesGetAll", func() {
		var (
			companies []interface{}
			page      int
			total     int
			limit     int
		)

		BeforeEach(func() {
			companies = []interface{}{}
			total = 1000
			limit = CompaniesDefaultSize
		})

		Context("with the default route", func() {
			BeforeEach(func() {
				for i := 0; i < total; i++ {
					company := &models.Company{
						ID:          uuid.New().String(),
						Name:        "Company",
						Description: "Description",
					}

					companies = append(companies, company)
				}

				datastore.On("CompaniesCount").Return(total, nil)
				datastore.On("CompaniesGetAll", AnythingOfType("int"), AnythingOfType("int")).Return(companies, nil)

				req, _ := http.NewRequest("GET", "/api/companies", nil)
				rw = httptest.NewRecorder()
				handler.CompaniesGetAll(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should have a jsonapi Content-Type", func() {
				Expect(rw.Header().Get("Content-Type")).To(Equal("application/vnd.api+json"))
			})

			It("should return valid JSON", func() {
				buf := bytes.NewBuffer(nil)
				payload, _ := jsonapi.Marshal(companies)

				payload.(*jsonapi.ManyPayload).Meta = &jsonapi.Meta{
					"total": total,
					"limit": CompaniesDefaultSize,
				}

				err := json.NewEncoder(buf).Encode(payload)
				Expect(err).NotTo(HaveOccurred())

				Expect(rw.Body).To(MatchJSON(buf))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("with a random page", func() {
			BeforeEach(func() {
				for i := 0; i < limit; i++ {
					company := &models.Company{
						ID:          uuid.New().String(),
						Name:        "Company",
						Description: "Description",
					}

					companies = append(companies, company)
				}

				datastore.On("CompaniesCount").Return(total, nil)
				datastore.On("CompaniesGetAll", AnythingOfType("int"), AnythingOfType("int")).Return(companies, nil)

				req, _ := http.NewRequest("GET", "/api/companies?page="+strconv.Itoa(page), nil)
				rw = httptest.NewRecorder()
				handler.CompaniesGetAll(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should have a jsonapi Content-Type", func() {
				Expect(rw.Header().Get("Content-Type")).To(Equal("application/vnd.api+json"))
			})

			It("should return valid JSON", func() {
				buf := bytes.NewBuffer(nil)
				payload, _ := jsonapi.Marshal(companies)

				payload.(*jsonapi.ManyPayload).Meta = &jsonapi.Meta{
					"total": total,
					"limit": CompaniesDefaultSize,
				}

				err := json.NewEncoder(buf).Encode(payload)
				Expect(err).NotTo(HaveOccurred())

				Expect(rw.Body).To(MatchJSON(buf))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("with a negative page parameter", func() {
			BeforeEach(func() {
				page = -5

				for i := 0; i < limit; i++ {
					company := &models.Company{
						ID:          uuid.New().String(),
						Name:        "Company",
						Description: "Description",
					}
					companies = append(companies, company)
				}

				datastore.On("CompaniesCount").Return(total, nil)
				datastore.On("CompaniesGetAll", AnythingOfType("int"), AnythingOfType("int")).Return(companies, nil)

				req, _ := http.NewRequest("GET", "/api/companies?page="+strconv.Itoa(page), nil)
				rw = httptest.NewRecorder()
				handler.CompaniesGetAll(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should have a jsonapi Content-Type", func() {
				Expect(rw.Header().Get("Content-Type")).To(Equal("application/vnd.api+json"))
			})

			It("should return valid JSON for the first page", func() {
				buf := bytes.NewBuffer(nil)
				payload, _ := jsonapi.Marshal(companies)

				payload.(*jsonapi.ManyPayload).Meta = &jsonapi.Meta{
					"total": total,
					"limit": CompaniesDefaultSize,
				}

				err := json.NewEncoder(buf).Encode(payload)
				Expect(err).NotTo(HaveOccurred())

				Expect(rw.Body).To(MatchJSON(buf))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("with an error calling CompaniesCount", func() {
			BeforeEach(func() {
				datastore.On("CompaniesCount").Return(0, errors.New("mock error"))

				req, _ := http.NewRequest("GET", "/api/companies?page="+strconv.Itoa(page), nil)
				rw = httptest.NewRecorder()
				handler.CompaniesGetAll(rw, req)
			})

			It("should return a 500 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusInternalServerError))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("with an error calling CompaniesGetAll", func() {
			BeforeEach(func() {
				datastore.On("CompaniesCount").Return(total, nil)
				datastore.On("CompaniesGetAll", AnythingOfType("int"), AnythingOfType("int")).Return(nil, errors.New("mock error"))

				req, _ := http.NewRequest("GET", "/api/companies?page="+strconv.Itoa(page), nil)
				rw = httptest.NewRecorder()
				handler.CompaniesGetAll(rw, req)
			})

			It("should return a 500 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusInternalServerError))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})
	})

	Describe("CompaniesGet", func() {
		var (
			company *models.Company
		)

		Context("with a normal request to CompaniesGet", func() {
			BeforeEach(func() {
				company = mocks.NewCompany()

				datastore.On("CompaniesGet", AnythingOfType("string")).Return(company, nil)

				req, _ := http.NewRequest("GET", fmt.Sprintf("/api/companies/%s", company.ID), nil)
				rw = httptest.NewRecorder()
				handler.CompaniesGet(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should have a jsonapi Content-Type", func() {
				Expect(rw.Header().Get("Content-Type")).To(Equal("application/vnd.api+json"))
			})

			It("should return valid JSON", func() {
				buf := bytes.NewBuffer(nil)
				err := jsonapi.MarshalPayload(buf, company)
				Expect(err).NotTo(HaveOccurred())

				Expect(rw.Body).To(MatchJSON(buf))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		// Context("with a garbage company id", func() {
		// 	BeforeEach(func() {
		// 		req, _ := http.NewRequest("GET", fmt.Sprintf("/api/companies/%s", "random-string"), nil)
		// 		rw = httptest.NewRecorder()
		// 		handler.CompaniesGet(rw, req)
		// 	})
		//
		// It("should invoke expected authorizer methods", func() {
		// 	authorizer.AssertExpectations(GinkgoT())
		// })
		//
		// It("should invoke expected datastore methods", func() {
		// 	datastore.AssertExpectations(GinkgoT())
		// })
		//
		// 	It("should return a 404 status code", func() {
		// 		Expect(rw.Code).To(Equal(http.StatusNoContent))
		// 	})
		// })

		Context("with an error calling CompaniesGet", func() {
			BeforeEach(func() {
				company = mocks.NewCompany()

				datastore.On("CompaniesGet", AnythingOfType("string")).Return(nil, errors.New("mock error"))

				req, _ := http.NewRequest("GET", fmt.Sprintf("/api/companies/%s", company.ID), nil)
				rw = httptest.NewRecorder()
				handler.CompaniesGet(rw, req)
			})

			It("should return a 404 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusNoContent))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})
	})
})
