package mocks

import (
	"math/rand"
	"strconv"

	"github.com/google/uuid"

	"gitlab.com/mrprincetonsdeals/backend/models"
)

// NewDeal creates a mock deal
func NewDeal() *models.Deal {
	return &models.Deal{
		ID:      uuid.New().String(),
		Code:    &models.Code{},
		Company: &models.Company{},
		Offer:   &models.Offer{},
		Friend:  &models.Friend{},
		UserID:  strconv.FormatInt(rand.Int63(), 10),
	}
}
