package main

import (
	"context"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/caarlos0/env"
	"github.com/rs/zerolog"

	"gitlab.com/mrprincetonsdeals/backend/handler"
	"gitlab.com/mrprincetonsdeals/backend/postgres"
	"gitlab.com/mrprincetonsdeals/backend/sessions"
)

const (
	// AppName is the current binary name
	AppName string = "referral-codes-web-app"
)

var (
	// GitSHA is the short git commit sha
	GitSHA string = ""
)

type config struct {
	BindAddress              string `env:"BIND_ADDRESS" envDefault:"0.0.0.0"`
	BindPort                 string `env:"BIND_PORT" envDefault:"3000"`
	DatabaseURL              string `env:"DATABASE_URL,required"`
	HostName                 string `env:"HOSTNAME,required"`
	InviteURL                string `env:"INVITE_URL,required"`
	SessionAuthenticationKey string `env:"SESSION_AUTH_KEY,required"`
	SessionEncryptionKey     string `env:"SESSION_ENCRYPTION_KEY,required"`
	SSLCert                  string `env:"SSL_CERT_PATH"`
	SSLKey                   string `env:"SSL_KEY_PATH"`
}

func main() {
	logger := zerolog.New(os.Stdout).With().Str("service", AppName).Str("git_sha", GitSHA).Timestamp().Logger()

	cfg := config{}
	err := env.Parse(&cfg)
	if err != nil {
		logger.Fatal().Err(err).Msg("unable to parse env config")
	}

	logger = logger.With().Str("host", cfg.HostName).Logger()

	datastore, err := postgres.Open(cfg.DatabaseURL)
	if err != nil {
		logger.Fatal().Err(err).Msg("opening database connection")
	}
	defer datastore.Close()

	authorizer := sessions.New(
		[]byte(cfg.SessionAuthenticationKey),
		[]byte(cfg.SessionEncryptionKey),
	)

	h := handler.New(datastore, authorizer, cfg.InviteURL, logger)
	addr := net.JoinHostPort(cfg.BindAddress, cfg.BindPort)

	server := &http.Server{
		Handler:        h.Router(),
		Addr:           addr,
		WriteTimeout:   60 * time.Second,
		ReadTimeout:    60 * time.Second,
		IdleTimeout:    120 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	go func() {
		logger.Info().Str("bind_address", addr).Msg("server started")
		err = server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			logger.Fatal().Err(err)
		}
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	<-stop

	logger.Info().Msg("server shutting down")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = server.Shutdown(ctx)
	if err != nil {
		logger.Fatal().Err(err)
	}

	logger.Info().Msg("server shut down complete")
}
