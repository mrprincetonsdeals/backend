package postgres

import (
	"gitlab.com/mrprincetonsdeals/backend/models"
)

// CodesCreate creates a code in the database
func (db *Datastore) CodesCreate(userID string, url string) (*models.Code, error) {
	var (
		code models.Code
		deal models.Deal
	)

	code.URL = url
	err := db.DB.QueryRow("INSERT INTO codes (url) VALUES ($1) RETURNING id;", url).Scan(&code.ID)
	if err != nil {
		return nil, err
	}

	offer, err := db.OffersGetByURL(url)
	if err != nil {
		return nil, err
	}

	if offer != nil {
		err = db.DB.QueryRow("INSERT INTO deals (code_id, user_id, offer_id, company_id) VALUES ($1, $2, $3, $4) RETURNING id;", code.ID, userID, offer.ID, offer.CompanyID).Scan(&deal.ID)
		if err != nil {
			return nil, err
		}
	} else {
		err = db.DB.QueryRow("INSERT INTO deals (code_id, user_id) VALUES ($1, $2) RETURNING id;", code.ID, userID).Scan(&deal.ID)
		if err != nil {
			return nil, err
		}
	}

	code.Deal = &deal

	return &code, nil
}

// CodesGet retrieves a single code by its uuid
func (db *Datastore) CodesGet(id string) (*models.Code, error) {
	row := db.DB.QueryRow("SELECT id, url FROM codes WHERE id = $1", id)
	var code models.Code
	err := row.Scan(&code.ID, &code.URL)

	return &code, err
}
