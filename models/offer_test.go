package models_test

import (
	"bytes"
	"fmt"

	"github.com/google/jsonapi"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var _ = Describe("Offer", func() {
	var (
		buf                *bytes.Buffer
		err                error
		offerID            string
		description        string
		companyID          string
		companyDescription string
	)

	BeforeEach(func() {
		companyID = uuid.New().String()
		companyDescription = "Company Description"
		company := &models.Company{
			ID:          companyID,
			Name:        "Company Name",
			Description: companyDescription,
		}

		offerID = uuid.New().String()
		description = "Offer Description"
		offer := &models.Offer{
			ID:          offerID,
			Description: description,
			CompanyID:   companyID,
			Company:     company,
		}

		buf = bytes.NewBuffer(nil)
		err = jsonapi.MarshalPayload(buf, offer)
	})

	It("should not have any errors", func() {
		Expect(err).NotTo(HaveOccurred())
	})

	It("should have the correct type", func() {
		Expect(buf.String()).To(ContainSubstring(`"type":"offers"`))
	})

	It("should have the correct offer id", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"id":"%s"`, offerID)))
	})

	It("should have the correct attributes", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"attributes":{"company-id":"%s","description":"%s"}`, companyID, description)))
	})

	It("should include the company type", func() {
		Expect(buf.String()).To(ContainSubstring(`"type":"companies"`))
	})

	It("should have the correct company id", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"type":"companies","id":"%s"`, companyID)))
	})
})
