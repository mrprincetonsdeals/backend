package handler_test

import (
	"context"
	"errors"
	"net/http"
	"net/http/httptest"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/rs/zerolog"
	. "github.com/stretchr/testify/mock"

	"gitlab.com/mrprincetonsdeals/backend/handler"
	. "gitlab.com/mrprincetonsdeals/backend/handler"
	"gitlab.com/mrprincetonsdeals/backend/mocks"
)

var _ = Describe("JWT Handlers", func() {
	var (
		handler    *handler.Handler
		authorizer *mocks.Authorizer
		rw         *httptest.ResponseRecorder
	)

	BeforeEach(func() {
		authorizer = &mocks.Authorizer{}
		datastore := &mocks.Datastore{}
		writer := &mocks.Writer{}
		writer.On("Write", Anything).Return(0, nil)
		logger := zerolog.New(writer)
		inviteURL := "http://example.com/friends/new?invite="

		handler = New(datastore, authorizer, inviteURL, logger)
	})

	Describe("WithRequiredAuthorization", func() {
		Context("with a valid auth token", func() {
			BeforeEach(func(done Done) {
				authorizer.On("ContextWithUserID", Anything, Anything).Return(context.TODO(), nil)

				successHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusOK)
					close(done)
				})

				req, _ := http.NewRequest("GET", "localhost/anyroute", nil)

				rw = httptest.NewRecorder()
				h := handler.WithRequiredAuthorization(successHandler)
				h.ServeHTTP(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})
		})

		Context("with an invalid auth token", func() {
			BeforeEach(func() {
				authorizer.On("ContextWithUserID", Anything, Anything).Return(context.TODO(), errors.New("mock error"))

				successHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					Fail("Should never return a status code")
				})

				req, _ := http.NewRequest("GET", "localhost/anyroute", nil)

				rw = httptest.NewRecorder()
				h := handler.WithRequiredAuthorization(successHandler)
				h.ServeHTTP(rw, req)
			})

			It("should return a 401 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusUnauthorized))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})
		})
	})

	Describe("WithOptionalAuthorization", func() {
		Context("with a valid auth token", func() {
			BeforeEach(func(done Done) {
				authorizer.On("ContextWithUserID", Anything, Anything).Return(context.TODO(), nil)

				successHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusOK)
					close(done)
				})

				req, _ := http.NewRequest("GET", "localhost/anyroute", nil)

				rw = httptest.NewRecorder()
				h := handler.WithOptionalAuthorization(successHandler)
				h.ServeHTTP(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})
		})

		Context("with an invalid auth token", func() {
			BeforeEach(func(done Done) {
				authorizer.On("ContextWithUserID", Anything, Anything).Return(context.TODO(), errors.New("mock error"))

				successHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusOK)
					close(done)
				})

				req, _ := http.NewRequest("GET", "localhost/anyroute", nil)

				rw = httptest.NewRecorder()
				h := handler.WithOptionalAuthorization(successHandler)
				h.ServeHTTP(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})
		})
	})
})
