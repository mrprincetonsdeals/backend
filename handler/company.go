package handler

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/google/jsonapi"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/hlog"
)

// CompaniesDefaultSize default size to page companies
const CompaniesDefaultSize = 20

// CompaniesGetAll route that handles
func (h *Handler) CompaniesGetAll(w http.ResponseWriter, r *http.Request) {
	limit := CompaniesDefaultSize
	offset := 0

	ll := hlog.FromRequest(r)

	total, err := h.DB.CompaniesCount()
	if err != nil {
		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	page, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err == nil && page >= 0 {
		offset = page * limit
	}

	companies, err := h.DB.CompaniesGetAll(limit, offset)
	if err != nil {
		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	payload, err := jsonapi.Marshal(companies)
	if err != nil {
		ll.Error().Err(err).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	payload.(*jsonapi.ManyPayload).Meta = &jsonapi.Meta{
		"total": total,
		"limit": limit,
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = json.NewEncoder(buf).Encode(payload)
	if err != nil {
		ll.Error().Err(err).Msg(JSONMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/vnd.api+json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}
}

// CompaniesGet route that retrieves a company by its uuid
func (h *Handler) CompaniesGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	companyID := vars["uuid"]

	ll := hlog.FromRequest(r)

	company, err := h.DB.CompaniesGet(companyID)
	if err != nil {
		ll.Error().Err(err).Str("company_id", companyID).Msg(DatastoreErrLogMsg)
		http.Error(w, "Not Found", http.StatusNoContent)
		return
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, company)
	if err != nil {
		ll.Error().Err(err).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/vnd.api+json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}
}
