package postgres

import (
	"database/sql"

	"github.com/lib/pq"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

// DealDelete marks a deal as deleted for a particular user
func (db *Datastore) DealDelete(userID string, dealID string) error {
	_, err := db.DB.Exec("UPDATE deals SET is_deleted = true WHERE user_id=$1 AND id=$2;", userID, dealID)
	if err != nil {
		return err
	}

	return nil
}

// DealsFindByUserID retrieves all deals by the user's uuid
func (db *Datastore) DealsFindByUserID(userID string) ([]interface{}, error) {
	rows, err := db.DB.Query("SELECT deals.id, deals.company_id, codes.id, codes.url FROM deals INNER JOIN codes ON (deals.code_id = codes.id) WHERE user_id = $1 AND is_deleted = false", userID)
	if err != nil {
		return nil, err
	}

	var (
		results    []interface{}
		companyIDs []string
	)

	for rows.Next() {
		var (
			deal      models.Deal
			code      models.Code
			companyID sql.NullString
		)

		if err := rows.Scan(&deal.ID, &companyID, &code.ID, &code.URL); err != nil {
			return nil, err
		}

		deal.Code = &code
		if companyID.Valid {
			deal.CompanyID = companyID.String
			companyIDs = append(companyIDs, deal.CompanyID)
		}

		results = append(results, &deal)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	rows, err = db.DB.Query("SELECT id, name, description FROM companies WHERE id = ANY($1)", pq.Array(companyIDs))
	if err != nil {
		return nil, err
	}

	companies := map[string]*models.Company{}
	for rows.Next() {
		var company models.Company
		if err := rows.Scan(&company.ID, &company.Name, &company.Description); err != nil {
			return nil, err
		}
		companies[company.ID] = &company
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	for _, dealInterface := range results {
		deal, ok := dealInterface.(*models.Deal)
		if !ok {
			continue
		}

		company, ok := companies[deal.CompanyID]
		if !ok {
			continue
		}
		deal.Company = company
	}

	return results, nil
}

// DealsFindByFriendsWithOfferID retrieves all deals owned by a user's friends matching an offer
func (db *Datastore) DealsFindByFriendsWithOfferID(userID string, offerID string) ([]*models.Deal, error) {
	rows, err := db.DB.Query("SELECT users.id, users.username, deals.id, deals.code_id FROM friends INNER JOIN users ON friends.friend_id = users.id INNER JOIN deals ON friends.friend_id = deals.user_id WHERE friends.user_id = $1 AND deals.offer_id = $2 AND deals.is_deleted = false;", userID, offerID)

	if err != nil {
		return nil, err
	}

	var (
		results []*models.Deal
	)

	for rows.Next() {
		var (
			deal   models.Deal
			friend models.Friend
		)

		if err := rows.Scan(&friend.ID, &friend.Name, &deal.ID, &deal.CodeID); err != nil {
			return nil, err
		}

		deal.Friend = &friend

		results = append(results, &deal)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return results, nil
}
