package mocks

import (
	"fmt"
	"math/rand"

	"github.com/google/uuid"

	"gitlab.com/mrprincetonsdeals/backend/models"
)

// NewCompany creates a mock company
func NewCompany() *models.Company {
	return &models.Company{
		ID:          uuid.New().String(),
		Name:        fmt.Sprintf("company-name-%d", rand.Int63()),
		Description: fmt.Sprintf("company-description-%d", rand.Int63()),
		LogoURL:     fmt.Sprintf("https://%s.com", "fakeurl"),
	}
}
