package models

import (
	"time"
)

// Deal is the model bridging ownership of a Code to both a Company (via the Offer) and a User
type Deal struct {
	ID        string   `jsonapi:"primary,deals"`
	CodeID    string   `jsonapi:"attr,code-id,omitempty"`
	Code      *Code    `jsonapi:"relation,code,omitempty"`
	CompanyID string   `jsonapi:"attr,company-id,omitempty"`
	Company   *Company `jsonapi:"relation,company,omitempty"`
	OfferID   string   `jsonapi:"attr,offer-id,omitempty"`
	Offer     *Offer   `jsonapi:"relation,offer,omitempty"`
	Friend    *Friend  `jsonapi:"relation,friend,omitempty"`
	UserID    string
	CreatedAt time.Time
	UpdatedAt time.Time
}
