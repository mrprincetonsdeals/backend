package models_test

import (
	"bytes"
	"fmt"

	"github.com/google/jsonapi"
	"github.com/google/uuid"
	"gitlab.com/mrprincetonsdeals/backend/models"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Code", func() {
	var (
		buf    *bytes.Buffer
		err    error
		codeID string
		url    string
	)

	BeforeEach(func() {
		codeID = uuid.New().String()
		url = "https://test.com"

		code := &models.Code{
			ID:  codeID,
			URL: url,
		}

		buf = bytes.NewBuffer(nil)
		err = jsonapi.MarshalPayload(buf, code)
	})

	It("should not have any errors", func() {
		Expect(err).NotTo(HaveOccurred())
	})

	It("should have the correct type", func() {
		Expect(buf.String()).To(ContainSubstring("\"type\":\"codes\""))
	})

	It("should have the correct id", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf("\"id\":\"%s\"", codeID)))
	})

	It("should have the correct attributes", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf("\"attributes\":{\"url\":\"%s\"}", url)))
	})
})
