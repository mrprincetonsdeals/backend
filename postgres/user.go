package postgres

import (
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var (
	// ErrUnknownVerify returned when an unverified user no longer exists
	ErrUnknownVerify = errors.New("unknown verify token")
)

// UsersCreate creates a new unverified user and returns the verification token
func (db *Datastore) UsersCreate(user *models.User) (string, error) {
	friendCode, err := db.CreateFriendCode()
	if err != nil {
		return "", errors.Wrap(err, "create friend code")
	}

	verificationToken, err := db.CreateVerificationToken()
	if err != nil {
		return "", errors.Wrap(err, "create friend code")
	}

	query := "INSERT INTO users (email, username, salt, password, friend_code, verification_token) VALUES ($1, $2, $3, $4, $5, $6);"
	_, err = db.DB.Exec(query, user.Email, user.Username, user.Salt, user.HashedPassword, friendCode, verificationToken)
	if err != nil {
		return "", errors.Wrap(err, "inserting into users")
	}

	return verificationToken, nil
}

// UsersVerify verifies a user's email address via token and creates a new user
func (db *Datastore) UsersVerify(verifyToken string) error {
	query := "UPDATE users SET is_verified = true, verification_token = NULL WHERE verification_token = $1;"
	_, err := db.DB.Exec(query, verifyToken)
	if err != nil {
		return errors.Wrap(err, "query veryify user")
	}

	return nil
}

// UsersGet retrieves a single user by email address
func (db *Datastore) UsersGet(email string) (*models.User, error) {
	user := &models.User{}

	query := "SELECT id, username, email, salt, password FROM users WHERE email = $1;"
	err := db.DB.QueryRow(query, email).Scan(&user.ID, &user.Username, &user.Email, &user.Salt, &user.HashedPassword)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}

		return nil, errors.Wrap(err, "select form users")
	}

	return user, nil
}

// UsersGetFriendCode retrieves a single friend code by user id
func (db *Datastore) UsersGetFriendCode(userID string) (string, error) {
	var friendCode string

	query := "SELECT friend_code FROM users WHERE id = $1;"
	err := db.DB.QueryRow(query, userID).Scan(&friendCode)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return "", nil
		}

		return "", err
	}

	return friendCode, nil
}

// UsersGetByFriendCode retrieves the user's name based on their friend code
func (db *Datastore) UsersGetByFriendCode(friendCode string) (*models.User, error) {
	user := &models.User{}

	query := "SELECT id, username FROM users WHERE friend_code = $1;"
	err := db.DB.QueryRow(query, friendCode).Scan(&user.ID, &user.Username)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}

		return nil, err
	}

	return user, nil
}

// CreateVerificationToken generates a new invite code for a user and stores it on the db.friendCodesMap
func (db *Datastore) CreateVerificationToken() (string, error) {
	var (
		stored = false
		token  string
		err    error
	)

	for !stored {
		token, err = db.VerificationTokenCreator()
		if err != nil {
			return "", errors.Wrap(err, "friend code create")
		}

		_, loaded := db.verificationTokens.LoadOrStore(token, struct{}{})
		stored = !loaded
	}

	return token, nil
}

// UpdateVerificationTokensMap retrieves all friend codes from the database
func (db *Datastore) UpdateVerificationTokensMap() error {
	query := "SELECT verification_token FROM users WHERE verification_token IS NOT NULL;"

	rows, err := db.DB.Query(query)
	if err != nil {
		return err
	}

	for rows.Next() {
		var token string
		if err := rows.Scan(&token); err != nil {
			return err
		}

		db.verificationTokens.Store(token, struct{}{})
	}

	if err := rows.Err(); err != nil {
		return err
	}

	return nil
}
