package sessions

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/gorilla/sessions"
)

type key int

const (
	// UserIDCtxKey is the key to add the user id to the request context
	UserIDCtxKey key = 0
	// UserIDCookieKey is the key to add the user id to the session cookie
	UserIDCookieKey string = "user_id"
	// AuthCookieName is the name for the cookie to store the JSON Web Token
	AuthCookieName string = "mrp_session"
)

var (
	// ErrContextInvalidIDType returned when the UserIDKey is present in the context but is not a string
	ErrContextInvalidIDType error = errors.New("context contained a user id with an invalid type")
	// ErrContextMissingID returned when the user id is not on the context
	ErrContextMissingID error = errors.New("context did not contain a user id")
	// ErrNilRequest returned when a request is nil
	ErrNilRequest error = errors.New("request was nil")
)

type Authorizer struct {
	Store *sessions.CookieStore
}

func New(keyPairs ...[]byte) *Authorizer {
	return &Authorizer{
		Store: sessions.NewCookieStore(keyPairs...),
	}
}

func (a *Authorizer) AddAuthCookieToRequest(w http.ResponseWriter, r *http.Request, userID string) error {
	// Ignore the error. An error will be returned if there is not an existing session. Store.Get always creates
	// a new session if one does
	session, _ := a.Store.Get(r, AuthCookieName)

	session.Values[UserIDCookieKey] = userID

	err := session.Save(r, w)
	if err != nil {
		return fmt.Errorf("saving cookie: %w", err)
	}

	return nil
}

func (a *Authorizer) ContextWithUserID(ctx context.Context, r *http.Request) (context.Context, error) {
	if r == nil {
		return ctx, ErrNilRequest
	}

	session, err := a.Store.Get(r, AuthCookieName)
	if err != nil {
		return ctx, err
	}

	id := session.Values[UserIDCookieKey]

	return context.WithValue(ctx, UserIDCtxKey, id), nil
}

func (a *Authorizer) UserIDFromContext(ctx context.Context) (string, error) {
	id := ctx.Value(UserIDCtxKey)

	if id == nil {
		return "", ErrContextMissingID
	}

	safeID, ok := id.(string)
	if !ok {
		return "", ErrContextInvalidIDType
	}

	if safeID == "" {
		return "", ErrContextMissingID
	}

	return safeID, nil
}
