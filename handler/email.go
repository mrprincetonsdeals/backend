package handler

// Emailer interface defining the email methods
type Emailer interface {
	NewRegistrationEmail(name, email, token string) error
}
