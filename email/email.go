package email

import (
	"errors"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

const (
	registrationEmailTemplateIDKey = "registration"
)

// Client implements sending emails
type Client struct {
	from      *mail.Email
	apiKey    string
	templates map[string]string
}

// NewClient creates a new email client
func NewClient(emailInfo, apiKey string, templates map[string]string) (*Client, error) {
	from, err := mail.ParseEmail(emailInfo)
	if err != nil {
		return nil, err
	}

	ec := &Client{
		from:      from,
		apiKey:    apiKey,
		templates: templates,
	}

	return ec, nil
}

// SendEmail sends the bytes of an email to SendGrid
func (ec *Client) SendEmail(body []byte) error {
	req := sendgrid.GetRequest(ec.apiKey, "/v3/mail/send", "https://api.sendgrid.com")
	req.Method = "POST"
	req.Body = body

	_, err := sendgrid.API(req)
	if err != nil {
		return err
	}

	return nil
}

// NewRegistrationEmail sends an new registration email
func (ec *Client) NewRegistrationEmail(name, email, token string) error {
	m := mail.NewV3Mail()

	m.SetFrom(ec.from)

	templateID, ok := ec.templates[registrationEmailTemplateIDKey]
	if !ok {
		return errors.New("missing registration email template id")
	}
	m.SetTemplateID(templateID)

	p := mail.NewPersonalization()

	to := mail.NewEmail(name, "test1@example.com")
	p.AddTos(to)
	p.SetDynamicTemplateData("receipt", "true")

	m.AddPersonalizations(p)

	body := mail.GetRequestBody(m)

	return ec.SendEmail(body)
}
