package handler

import (
	"bytes"
	"net/http"

	"github.com/google/jsonapi"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/hlog"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

// func DealIsValid(deal models.Deal) ERROR: {
// 	if deal.Code == nil {
// 		return InvalidLinkError
// 	}
// }

// CodesCreate route that creates a code for a user
func (h *Handler) CodesCreate(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	userID, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Error().Err(err).Msg(UnauthorizedUserLogMsg)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	var code models.Code
	defer r.Body.Close()
	err = jsonapi.UnmarshalPayload(r.Body, &code)
	if err != nil {
		ll.Error().Err(err).Msg(UnmarshallingRequestErrLogMsg)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	// err = DealIsValid(deal)
	// if err != nil {
	// 	http.Error(w, err.Error(), http.StatusBadRequest)
	// 	return
	// }

	newCode, err := h.DB.CodesCreate(userID, code.URL)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Str("url", code.URL).Msg(DatastoreErrLogMsg)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, newCode)
	if err != nil {
		ll.Error().Err(err).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/vnd.api+json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}
}

// CodesGet route that retrieves a code for a user by its uuid
func (h *Handler) CodesGet(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	_, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Printf("ERROR Unauthorized request %s", err.Error())
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	id := vars["uuid"]

	code, err := h.DB.CodesGet(id)
	if err != nil {
		ll.Error().Str("code_id", id).Err(err).Msg(DatastoreErrLogMsg)
		http.Error(w, "", http.StatusNoContent)
		return
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, code)
	if err != nil {
		ll.Error().Err(err).Msg(JSONMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/vnd.api+json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}
}
