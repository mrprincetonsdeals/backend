package models

import "time"

// Offer joins a Deal (owned by a User) to a Company and describes what the Deal offers
type Offer struct {
	ID          string `jsonapi:"primary,offers"`
	Description string `jsonapi:"attr,description"`
	URLPrefix   string
	Deals       []*Deal  `jsonapi:"relation,deals,omitempty"`
	CompanyID   string   `jsonapi:"attr,company-id,omitempty"`
	Company     *Company `jsonapi:"relation,company,omitempty"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
