package handler

import (
	"context"
	"net/http"
)

// Authorizer interface defining the auth methods
type Authorizer interface {
	AddAuthCookieToRequest(w http.ResponseWriter, r *http.Request, userID string) error
	ContextWithUserID(ctx context.Context, r *http.Request) (context.Context, error)
	UserIDFromContext(ctx context.Context) (string, error)
}
