package handler_test

import (
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strconv"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/rs/zerolog"
	. "github.com/stretchr/testify/mock"
	. "gitlab.com/mrprincetonsdeals/backend/handler"
	"gitlab.com/mrprincetonsdeals/backend/mocks"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var _ = Describe("Deal Handler", func() {
	var (
		handler    *Handler
		datastore  *mocks.Datastore
		authorizer *mocks.Authorizer
		writer     *mocks.Writer
		rw         *httptest.ResponseRecorder
	)

	BeforeEach(func() {
		datastore = &mocks.Datastore{}
		authorizer = &mocks.Authorizer{}
		writer = &mocks.Writer{}
		writer.On("Write", Anything).Return(0, nil)
		logger := zerolog.New(writer)
		inviteURL := "http://example.com/friends/new?invite="

		handler = New(datastore, authorizer, inviteURL, logger)
	})

	Describe("DealsDelete", func() {
		var (
			deal *models.Deal
		)

		BeforeEach(func() {
			deal = mocks.NewDeal()
		})

		Context("with a normal request to DealsDelete", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return(deal.UserID, nil)
				datastore.On("DealDelete", AnythingOfType("string"), AnythingOfType("string")).Return(nil)

				req, _ := http.NewRequest("DELETE", fmt.Sprintf("/api/deals/%s", deal.ID), nil)
				rw = httptest.NewRecorder()
				handler.DealsDelete(rw, req)
			})

			It("should return a 204 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusNoContent))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("with a deal id that doesn't exist", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return(deal.UserID, nil)
				datastore.On("DealDelete", AnythingOfType("string"), AnythingOfType("string")).Return(errors.New("mock error"))

				req, _ := http.NewRequest("DELETE", fmt.Sprintf("/api/deals/%s", deal.ID), nil)
				rw = httptest.NewRecorder()
				handler.DealsDelete(rw, req)
			})

			It("should return a 404 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusNoContent))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("without a valid user id", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return("", errors.New("mock error"))

				req, _ := http.NewRequest("DELETE", fmt.Sprintf("/api/deals/%s", deal.ID), nil)
				rw = httptest.NewRecorder()
				handler.DealsDelete(rw, req)
			})

			It("should return a 401 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusUnauthorized))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})
	})

	Describe("DealsGetAll", func() {
		var (
			deals  []interface{}
			total  int
			userID string
		)

		BeforeEach(func() {
			deals = []interface{}{}
			total = 1000
			userID = strconv.FormatInt(rand.Int63(), 10)
		})

		Context("with a valid user id", func() {
			BeforeEach(func() {
				for i := 0; i < total; i++ {
					deal := mocks.NewDeal()
					deal.UserID = userID
					deals = append(deals, deal)
				}

				authorizer.On("UserIDFromContext", Anything).Return(userID, nil)
				datastore.On("DealsFindByUserID", AnythingOfType("string")).Return(deals, nil)

				req, _ := http.NewRequest("GET", "/api/deals", nil)
				rw = httptest.NewRecorder()
				handler.DealsGetAll(rw, req)
			})

			It("should return a 200 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusOK))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("without a valid user id", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return("", errors.New("mock error"))

				req, _ := http.NewRequest("GET", "/api/deals", nil)
				rw = httptest.NewRecorder()
				handler.DealsGetAll(rw, req)
			})

			It("should return a 401 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusUnauthorized))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})

		Context("with an error in DealsFindByUserID", func() {
			BeforeEach(func() {
				authorizer.On("UserIDFromContext", Anything).Return(userID, nil)
				datastore.On("DealsFindByUserID", AnythingOfType("string")).Return(nil, errors.New("mock error"))

				req, _ := http.NewRequest("GET", "/api/deals", nil)
				rw = httptest.NewRecorder()
				handler.DealsGetAll(rw, req)
			})

			It("should return a 500 status code", func() {
				Expect(rw.Code).To(Equal(http.StatusInternalServerError))
			})

			It("should invoke expected authorizer methods", func() {
				authorizer.AssertExpectations(GinkgoT())
			})

			It("should invoke expected datastore methods", func() {
				datastore.AssertExpectations(GinkgoT())
			})
		})
	})
})
