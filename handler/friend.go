package handler

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/google/jsonapi"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/hlog"
)

// FriendsDefaultSize default size to page friends
const FriendsDefaultSize = 20

// FriendsGetAll route that retrieves all friends for a user
func (h *Handler) FriendsGetAll(w http.ResponseWriter, r *http.Request) {
	limit := FriendsDefaultSize
	offset := 0

	ll := hlog.FromRequest(r)

	userID, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Error().Err(err).Msg(UnauthorizedUserLogMsg)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	total, err := h.DB.FriendsCount(userID)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	page, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err == nil && page >= 0 {
		offset = page * limit
	}

	friends, err := h.DB.FriendsGetAll(userID, limit, offset)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	payload, err := jsonapi.Marshal(friends)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	payload.(*jsonapi.ManyPayload).Meta = &jsonapi.Meta{
		"total": total,
		"limit": limit,
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = json.NewEncoder(buf).Encode(payload)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Msg(JSONMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/vnd.api+json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}
}

// FriendsGet route that retrieves a friend for a user by their uuid
func (h *Handler) FriendsGet(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	userID, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Error().Err(err).Msg(UnauthorizedUserLogMsg)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	friendID := vars["uuid"]

	friend, err := h.DB.FriendsGet(userID, friendID)
	if err != nil || friend == nil {
		ll.Error().Err(err).Str("friend_id", friendID).Msg(DatastoreErrLogMsg)
		http.Error(w, "Not Found", http.StatusNoContent)
		return
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, friend)
	if err != nil {
		ll.Error().Err(err).Str("friend_id", friendID).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/vnd.api+json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}
}
