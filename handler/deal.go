package handler

import (
	"bytes"
	"net/http"

	"github.com/google/jsonapi"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/hlog"
)

// DealsDelete route that marks a deal as deleted by its uuid
func (h *Handler) DealsDelete(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	userID, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Error().Err(err).Msg(UnauthorizedUserLogMsg)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	dealID := vars["uuid"]

	err = h.DB.DealDelete(userID, dealID)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Str("deal_id", dealID).Msg(DatastoreErrLogMsg)
		http.Error(w, "Not Found", http.StatusNoContent)
		return
	}

	w.WriteHeader(http.StatusNoContent)
	w.Header().Set("Content-Type", "application/vnd.api+json")
}

// DealsGetAll route that retrieves all deals for a user
func (h *Handler) DealsGetAll(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	userID, err := h.Authorizer.UserIDFromContext(r.Context())
	if err != nil {
		ll.Error().Err(err).Msg(UnauthorizedUserLogMsg)
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	deals, err := h.DB.DealsFindByUserID(userID)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Msg(DatastoreErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	buf := h.ResponseBufferPool.Get().(*bytes.Buffer)
	defer h.ResponseBufferPool.Put(buf)
	buf.Reset()

	err = jsonapi.MarshalPayload(buf, deals)
	if err != nil {
		ll.Error().Err(err).Str("user_id", userID).Msg(JSONAPIMarshallingResponseErrLogMsg)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/vnd.api+json")
	_, err = w.Write(buf.Bytes())
	if err != nil {
		ll.Error().Err(err).Msg(HTTPWriteErrLogMsg)
	}
}
