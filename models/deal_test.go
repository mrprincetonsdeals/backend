package models_test

import (
	"bytes"
	"fmt"

	"github.com/google/jsonapi"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/mrprincetonsdeals/backend/models"
)

var _ = Describe("Deal", func() {
	var (
		buf       *bytes.Buffer
		err       error
		dealID    string
		companyID string
	)

	BeforeEach(func() {
		dealID = uuid.New().String()
		companyID = uuid.New().String()

		deal := &models.Deal{
			ID:        dealID,
			CompanyID: companyID,
		}

		buf = bytes.NewBuffer(nil)
		err = jsonapi.MarshalPayload(buf, deal)
	})

	It("should not have any errors", func() {
		Expect(err).NotTo(HaveOccurred())
	})

	It("should have the correct type", func() {
		Expect(buf.String()).To(ContainSubstring(`"type":"deals"`))
	})

	It("should have the correct id", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"id":"%s"`, dealID)))
	})

	It("should have the correct attributes", func() {
		Expect(buf.String()).To(ContainSubstring(fmt.Sprintf(`"attributes":{"company-id":"%s"}`, companyID)))
	})
})
