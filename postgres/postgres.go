package postgres

import (
	"database/sql"
	"sync"

	// Import database client library
	_ "github.com/lib/pq"
	"gitlab.com/mrprincetonsdeals/backend/rand"
)

// Datastore struct for accessing the datastore backing the web app
type Datastore struct {
	DB                       *sql.DB
	OffersMatcher            map[string]string
	FriendCodeCreator        func() (string, error)
	friendCodes              sync.Map
	VerificationTokenCreator func() (string, error)
	verificationTokens       sync.Map
	mux                      sync.RWMutex
}

// Open creates the database connection and returns a db
func Open(databaseURL string) (*Datastore, error) {
	db, err := sql.Open("postgres", databaseURL)
	if err != nil {
		return nil, err
	}

	ds := &Datastore{
		DB:                       db,
		OffersMatcher:            map[string]string{},
		FriendCodeCreator:        rand.NewURLSafeToken,
		VerificationTokenCreator: rand.NewURLSafeToken,
	}

	err = ds.UpdateOffersMap()
	if err != nil {
		return nil, err
	}

	err = ds.UpdateFriendCodesMap()
	if err != nil {
		return nil, err
	}

	err = ds.UpdateVerificationTokensMap()
	if err != nil {
		return nil, err
	}

	return ds, nil
}

// Close closes down the database connection
func (db *Datastore) Close() {
	db.DB.Close()
}

// Ping tests that the database connection is still alive
func (db *Datastore) Ping() error {
	return db.DB.Ping()
}
