-- Migration: create-users
-- Created at: 2018-11-21 16:38:54
-- ====  UP  ====

BEGIN;

  CREATE EXTENSION IF NOT EXISTS pgcrypto;

  CREATE TABLE users (
    id                 uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    username           text NOT NULL UNIQUE,
    email              text NOT NULL UNIQUE,
    salt               bytea NOT NULL,
    password           bytea NOT NULL,
    friend_code        text NOT NULL UNIQUE,
    verification_token text UNIQUE,
    is_verified        boolean NOT NULL DEFAULT false,
    invited_by         text REFERENCES users(friend_code),
    created_at         timestamptz NOT NULL DEFAULT now(),
    updated_at         timestamptz NOT NULL DEFAULT now()
  );

  CREATE TABLE friends (
    user_id     uuid NOT NULL REFERENCES users(id),
    friend_id   uuid NOT NULL REFERENCES users(id),
    is_deleted  boolean NOT NULL DEFAULT false,
    created_at  timestamptz NOT NULL DEFAULT now(),
    updated_at  timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY (user_id, friend_id)
  );

COMMIT;

-- ==== DOWN ====

BEGIN;

  DROP TABLE friends;
  DROP TABLE users;

COMMIT;
