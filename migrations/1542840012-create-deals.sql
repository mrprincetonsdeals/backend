-- Migration: create-deals
-- Created at: 2018-11-21 16:40:12
-- ====  UP  ====

BEGIN;

  CREATE EXTENSION IF NOT EXISTS pgcrypto;

  CREATE TABLE offers (
    id          uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    description text NOT NULL DEFAULT '',
    url_prefix  text NOT NULL,
    company_id  uuid NOT NULL REFERENCES companies(id),
    created_at  timestamptz NOT NULL DEFAULT now(),
    updated_at  timestamptz NOT NULL DEFAULT now()
  );

  CREATE TABLE recommended_offers (
    id SERIAL PRIMARY KEY NOT NULL,
    offer_id uuid NOT NULL REFERENCES offers(id),
    company_id  uuid NOT NULL REFERENCES companies(id)
  );

  CREATE TABLE codes (
    id  uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    url text NOT NULL
  );

  CREATE TABLE deals (
    id         uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    code_id    uuid NOT NULL REFERENCES codes(id),
    user_id    uuid NOT NULL REFERENCES users(id),
    offer_id   uuid REFERENCES offers(id),
    company_id uuid REFERENCES companies(id),
    is_deleted boolean NOT NULL DEFAULT false,
    created_at timestamptz NOT NULL DEFAULT now(),
    updated_at timestamptz NOT NULL DEFAULT now()
  );

  CREATE INDEX deals_active_idx ON deals (user_id, id) WHERE is_deleted = false;

COMMIT;

-- ==== DOWN ====

BEGIN;

  DROP TABLE deals;
  DROP TABLE recommended_offers;
  DROP TABLE offers;
  DROP TABLE codes;

COMMIT;
