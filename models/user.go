package models

import (
	"time"
)

// User tracks the login details for a user
type User struct {
	ID             string `jsonapi:"primary,users"`
	Username       string `jsonapi:"attr,username"`
	Email          string
	JWTVersion     int16
	Salt           []byte
	HashedPassword []byte
	CreatedAt      time.Time
	UpdatedAt      time.Time
}
