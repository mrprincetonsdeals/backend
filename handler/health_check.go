package handler

import (
	"net/http"

	"github.com/rs/zerolog/hlog"
)

// HealthCheck route that checks the connection to the database is still healthy
func (h *Handler) HealthCheck(w http.ResponseWriter, r *http.Request) {
	ll := hlog.FromRequest(r)

	err := h.DB.Ping()
	if err != nil {
		ll.Error().Err(err).Msg(DatastoreErrLogMsg)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
