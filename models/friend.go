package models

import "time"

// Friend is the model for User's friends
type Friend struct {
	ID        string  `jsonapi:"primary,friends"`
	Name      string  `jsonapi:"attr,name"`
	DealCount int     `jsonapi:"attr,deal-count,omitempty"`
	Deals     []*Deal `jsonapi:"relation,deals,omitempty"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
