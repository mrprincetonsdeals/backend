package mocks

import (
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/mrprincetonsdeals/backend/models"
)

// NewCode creates a mock Code
func NewCode() *models.Code {
	return &models.Code{
		ID:   uuid.New().String(),
		URL:  fmt.Sprintf("https://%s.com", "fakeurl"),
		Deal: &models.Deal{},
	}
}
