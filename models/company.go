package models

import (
	"time"
)

// Company is the model for the entity that offers referral codes
type Company struct {
	ID              string   `jsonapi:"primary,companies"`
	Name            string   `jsonapi:"attr,name"`
	SafeName        string   `jsonapi:"attr,safe-name"`
	Description     string   `jsonapi:"attr,description"`
	LogoURL         string   `jsonapi:"attr,logo-url"`
	Offers          []*Offer `jsonapi:"relation,offers,omitempty"`
	FriendsWithDeal int      `jsonapi:"attr,friends-with-deal,omitempty"`
	CreatedAt       time.Time
	UpdatedAt       time.Time
}
